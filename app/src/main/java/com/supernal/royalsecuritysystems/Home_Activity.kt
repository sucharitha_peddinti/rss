package com.supernal.royalsecuritysystems

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.supernal.royalsecuritysystems.databinding.ActivityHomeBinding
import com.supernal.royalsecuritysystems.models.ProfileResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Home_Activity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var sharedPreferences: SharedPreferences

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityHomeBinding
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    lateinit var navBottomView: BottomNavigationView
    lateinit var navController: NavController
    lateinit var mobile_header: TextView

    var url =
        "https://api.whatsapp.com/send?phone=+9191008888 38&&text=Hello! Welcome to Royal Security Systems"

    lateinit var fab_whatsap: ImageView

    lateinit var profile_name: TextView
    lateinit var profile_image: ImageView
    lateinit var userid: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarHome.toolbar)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        userid = sharedPreferences.getString("userid", "").toString()

        Log.e("user", userid)
        drawerLayout = binding.drawerLayout
        navView = binding.navView
        navController = findNavController(R.id.nav_host_fragment_content_home)
        navBottomView = findViewById(R.id.bottom_navigation_view)
        val headerView: View = binding.navView.getHeaderView(0)

        profile_name = headerView.findViewById(R.id.profile_name)
        profile_image = headerView.findViewById(R.id.profile_image)
        mobile_header = findViewById(R.id.mobile_header)
        fab_whatsap = findViewById(R.id.fab)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.navigation_services,
                R.id.navigation_invoices,
                R.id.navigation_support,
                R.id.nav_contactus
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navBottomView.setupWithNavController(navController)



        navView.setNavigationItemSelectedListener(this)
        getProfileDetails()




    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_home)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_whatsapp -> {
//                val i = Intent(Intent.ACTION_VIEW)
//                i.data = Uri.parse(url)
//                // Give your message here
//
//                startActivity(i)
                navController.navigate(R.id.navigation_notes)

                true
            }
            R.id.action_phone -> {

                val dialIntent = Intent(Intent.ACTION_DIAL)
                dialIntent.data = Uri.parse("tel:" + "9100888838")
                startActivity(dialIntent)
                return true
            }
            R.id.action_settings -> {

                navController.navigate(R.id.navigation_notification)

                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        item.isChecked = true
        drawerLayout.closeDrawers()

        val id = item.itemId

        when (id) {
            R.id.nav_logout -> logoutfun()
//            R.id.nav_settings -> navController.navigate(R.id.nav_settings)

            R.id.nav_services ->

                navController.navigate(R.id.navigation_services)
            R.id.nav_home ->

                navController.navigate(R.id.nav_home)
            R.id.nav_contactus ->

                navController.navigate(R.id.navigation_contactus)
            R.id.nav_myprofile ->

                navController.navigate(R.id.navigation_profile)
            R.id.navigation_terms ->

                navController.navigate(R.id.navigation_terms)
        }
        return true

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home_, menu)
        return true
    }


    private fun getProfileDetails() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.getProfileDetails(getString(R.string.api_key), userid)

            call.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())

                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        val profilelist =
                            response.body()?.response!!


                        profile_name.text = profilelist.name

                    }

//

                }


                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())

                }

            })


        } else {

            Toast.makeText(
                this,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    fun logoutfun() {

        sharedPreferences.edit().clear().commit()
        sharedPreferences.edit().clear().apply()

        intent = Intent(applicationContext, Login_Screen::class.java)
        startActivity(intent)
        finish()

    }


}