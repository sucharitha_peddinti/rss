package com.supernal.royalsecuritysystems

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.models.LoginListResponse
import com.erepairs.app.utils.NetWorkConection
import com.google.firebase.iid.FirebaseInstanceId
import com.supernal.royalsecuritysystems.databinding.OtpScreenBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Otp_Screen : AppCompatActivity() {

    private lateinit var binding: OtpScreenBinding
    lateinit var sharedPreferences: SharedPreferences

    lateinit var otp_edit: EditText
    lateinit var mobilenumber_textview: TextView
    lateinit var otp_stng: String
    lateinit var mobile_stng_prefs: String
    lateinit var progressBarotp: ProgressBar
    lateinit var submit_btn: Button
    lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = OtpScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        otp_edit = binding.otpBtn
        mobilenumber_textview = binding.mobilenumberTextview
        submit_btn = binding.submitBtn
        progressBarotp = binding.progressBarotp

        sharedPreferences = getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

        mobile_stng_prefs = sharedPreferences.getString("mobile_number", "").toString()

        mobilenumber_textview.text = "We Have Sent The OTP On $mobile_stng_prefs"

        //Get the token
        //Use the token only for received a push notification this device
        token = FirebaseInstanceId.getInstance().token.toString()
        Log.d(ContentValues.TAG, "Token : $token")

        submit_btn.setOnClickListener {

            otp_stng = otp_edit.text.toString()

            if (otp_stng.isEmpty()) {
                otp_edit.error = "Please Enter OTP"
            } else {
                postOTP()
            }

        }

    }

    private fun postOTP() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            //Set the Adapter to the RecyclerView//

            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.postotp(
                    getString(R.string.api_key),
                    mobile_stng_prefs,
                    otp_stng,token
                )
            Log.e("otp_stng", otp_stng)
            Log.e("mobile_stng_prefs", mobile_stng_prefs)

            progressBarotp.visibility = View.VISIBLE

            call.enqueue(object : Callback<LoginListResponse> {
                @SuppressLint("WrongConstant")
                override fun onResponse(
                    call: Call<LoginListResponse>,
                    response: Response<LoginListResponse>
                ) {

                    progressBarotp.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.toString())

                    val status = response.body()?.code
                    if ((status!!.equals("1") || (status == 1))) {

                        //Set the Adapter to the RecyclerView//

                        try {


                            val listOfcategories = response.body()?.response

                            Log.e("ot res", "" + listOfcategories)
//                            Toast.makeText(
//                                this@Otp_Screen,
//                                "" + response.body()?.response,
//                                Toast.LENGTH_LONG
//                            ).show()
                            val editor = sharedPreferences.edit()
                            editor.putString("userid", response.body()?.response!!.id.toString())
                            editor.putString("name", response.body()?.response!!.name.toString())
                            editor.putString("email", response.body()?.response!!.email.toString())
                            editor.putString("mobile", response.body()?.response!!.mobile.toString())
                            editor.putString("companyname", response.body()?.response!!.company.toString())
                            editor.putBoolean("islogin", true)
                            editor.commit()
                            val intent = Intent(this@Otp_Screen, Home_Activity::class.java)
                            startActivity(intent)
                            finish()

                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }


                    } else {
                        Toast.makeText(
                            this@Otp_Screen,
                            "" + response.body()?.response,
                            Toast.LENGTH_LONG
                        ).show()


                    }
                }


                override fun onFailure(call: Call<LoginListResponse>, t: Throwable) {
                    progressBarotp.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.toString())
                    Toast.makeText(
                        this@Otp_Screen,
                        "Invalid OTP",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }

}