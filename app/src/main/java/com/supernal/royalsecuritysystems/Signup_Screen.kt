package com.supernal.royalsecuritysystems

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.google.gson.JsonSyntaxException
import com.supernal.royalsecuritysystems.databinding.SignupScreenBinding
import com.supernal.royalsecuritysystems.models.LoginDefaultResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Signup_Screen : Activity() {

    private lateinit var binding: SignupScreenBinding
    lateinit var company_edit: EditText
    lateinit var company_strng: String
    lateinit var username_edit: EditText
    lateinit var username_strng: String
    lateinit var email_edit: EditText
    lateinit var gsit_edit: EditText
    lateinit var email_strng: String
    lateinit var gsit_strng: String
    lateinit var sigin_btn: TextView
    lateinit var address_strng: String
    lateinit var phonenumber_edit: EditText
    lateinit var phonenumber_strng: String
    lateinit var termscheck: CheckBox
    lateinit var progressBarregister: ProgressBar
    lateinit var sharedPreferences: SharedPreferences

    var pattern =
        "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"

    val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    lateinit var signup_btn: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SignupScreenBinding.inflate(layoutInflater)

        setContentView(binding.root)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        company_edit = binding.companyEdit
        gsit_edit = binding.gsitEdit
        username_edit = binding.usernameEdit
        email_edit = binding.emailEdit
        phonenumber_edit = binding.mobileEdit
        termscheck = binding.termscheck
        sigin_btn = binding.siginBtn
        signup_btn = binding.signupBtn
        progressBarregister = binding.progressBarregister

        termscheck.isChecked = false


        binding.signupBtn.setOnClickListener {
            company_strng = company_edit.text.toString()
            username_strng = username_edit.text.toString()
            email_strng = email_edit.text.toString()
            phonenumber_strng = phonenumber_edit.text.toString()
            gsit_strng = gsit_edit.text.toString()
            address_strng = binding.addressEdit.text.toString()


            if (company_strng.isEmpty()) {
                company_edit.error = "Please Enter Company Number"
            } else if (username_strng.isEmpty()) {
                username_edit.error = "Please Enter UserName"
            } else if (phonenumber_strng.isEmpty()) {
                phonenumber_edit.error = "Please Enter Phone Number"
            } else if (gsit_strng.isEmpty()) {
                gsit_edit.error = "Please Enter GSIT"
            } else if (address_strng.isEmpty()) {
                binding.addressEdit.error = "Please Enter Address"
            } else if (phonenumber_strng.length < 10) {
                phonenumber_edit.error = "Please Enter 10 digits Phone Number"

            } else if (!phonenumber_strng.matches(pattern.toRegex())) {
                phonenumber_edit.error = "Please Enter valid Phone Number"

            } else if (email_strng.isEmpty()) {
                email_edit.error = "Please Enter Email Address"
            } else if (!termscheck.isChecked) {
                Toast.makeText(this, "Please accept Terms and Conditions", Toast.LENGTH_LONG).show()
            } else {
                postRegister()
            }

        }
        binding.siginBtn.setOnClickListener {

            val intent = Intent(this, Login_Screen::class.java)
            startActivity(intent)
            finish()
        }


    }


    private fun postRegister() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            //Set the Adapter to the RecyclerView//

            var apiServices = APIClient.client.create(Api::class.java)
            val call =
                apiServices.postregister(
                    getString(R.string.api_key),
                    company_strng,
                    username_strng,
                    email_strng,

                    phonenumber_strng, gsit_strng,address_strng
                )
            Log.e("phonenumber_strng", phonenumber_strng)
            Log.e(
                "email_strng", "" + email_strng
            )

            progressBarregister.visibility = View.VISIBLE

            call.enqueue(object : Callback<LoginDefaultResponse> {
                override fun onResponse(
                    call: Call<LoginDefaultResponse>,
                    response: Response<LoginDefaultResponse>
                ) {
                    progressBarregister.visibility = View.GONE

                    Log.e(ContentValues.TAG, response.body()!!.response.toString())
                    try {
                        val status_code = response.body()?.code
                        val status = response.body()?.status
                        if ((status!!.equals("true")) || (status == true)) {

//                        if (response.isSuccessful) {
                            if ((status_code == 1) || (status_code!!.equals(1))) {


                                //Set the Adapter to the RecyclerView//


                                val listOfcategories = response.body()?.response

                                Log.e("res", "" + listOfcategories)


                                val intent = Intent(
                                    this@Signup_Screen,
                                    Login_Screen::class.java
                                )
                                startActivity(intent)
                                finish()


                            } else if ((status_code == 2) || (status_code.equals(2))) {
                                Toast.makeText(
                                    this@Signup_Screen,
                                    "" + response.body()?.message,
                                    Toast.LENGTH_LONG
                                ).show()

                            } else if ((status_code == 3) || (status_code.equals(3))) {
                                Toast.makeText(
                                    this@Signup_Screen,
                                    "" + response.body()?.message,
                                    Toast.LENGTH_LONG
                                ).show()

                            } else if ((status_code == 4) || (status_code.equals(4))) {
                                Toast.makeText(
                                    this@Signup_Screen,
                                    "" + response.body()?.message,
                                    Toast.LENGTH_LONG
                                ).show()

                            }
                        }
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    } catch (e: TypeCastException) {
                        e.printStackTrace()
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }


                }

                override fun onFailure(call: Call<LoginDefaultResponse>, t: Throwable) {
                    progressBarregister.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.localizedMessage)

                    val intent = Intent(
                        this@Signup_Screen,
                        Login_Screen::class.java
                    )
                    startActivity(intent)
                    finish()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }


//    private fun postRegister() {
//
//        if (NetWorkConection.isNEtworkConnected(this)) {
//
//            //Set the Adapter to the RecyclerView//
//
//
//            var apiServices = APIClient.client.create(Api::class.java)
//
//            val call =
//                apiServices.postregister(
//                    getString(R.string.api_key),
//                    company_strng,
//                    username_strng,
//                    phonenumber_strng,
//                    email_strng
//                )
//            Log.e("phonenumber_strng", phonenumber_strng)
//            Log.e("email_strng", email_strng)
//            progressBarregister.visibility = View.VISIBLE
//
//            call.enqueue(object : Callback<RegisterListResponse> {
//                override fun onResponse(
//                    call: Call<RegisterListResponse>,
//                    response: Response<RegisterListResponse>
//                ) {
//
//                    progressBarregister.visibility = View.GONE
//                    Log.e(ContentValues.TAG, response.body()!!.message)
//
////                    val fromUserJson = Gson().toJson(response.body()!!.response)
////                    val dataPoint:RegisterListResponse = Gson().fromJson(
////                        fromUserJson,
////                        RegisterListResponse::class.java
////                    )
////
////                    Log.e("dataPoint",""+dataPoint)
//
//                    val codee = response.body()?.code
//                    val status = response.body()?.status
//                    if ((status!!.equals("true") || (status == true))) {
//                        if ((codee!!.equals("1") || (codee == 1))) {
//
//                            //Set the Adapter to the RecyclerView//
//
//                            try {
//
//
//                                Toast.makeText(
//                                    this@Signup_Screen,
//                                    "" + response.body()?.message,
//                                    Toast.LENGTH_LONG
//                                ).show()
//
//                                val intent = Intent(
//                                    this@Signup_Screen,
//                                    Login_Screen::class.java
//                                )
//                                startActivity(intent)
//                                finish()
//                                val editor = sharedPreferences.edit()
//                                editor.putString("mobilenumber", response.body()!!.response.mobile)
//                                Log.e("mobile", "" + response.body()!!.response.mobile)
//                                editor.commit()
//
//
//                            } catch (e: NullPointerException) {
//                                e.printStackTrace()
//                            } catch (e: TypeCastException) {
//                                e.printStackTrace()
//                            }
//
//                        }
//
//                    } else if ((codee == 2) || (codee!!.equals(2))) {
//
//                        Toast.makeText(
//                            this@Signup_Screen,
//                            "" + response.body()?.message,
//                            Toast.LENGTH_LONG
//                        ).show()
//
//                    } else if ((codee == 3) || (codee.equals(3))) {
//
//                        Toast.makeText(
//                            this@Signup_Screen,
//                            "" + response.body()?.message,
//                            Toast.LENGTH_LONG
//                        ).show()
//
//                    } else {
//                        Toast.makeText(
//                            this@Signup_Screen,
//                            "" + response.body()?.message,
//                            Toast.LENGTH_LONG
//                        ).show()
//
//                    }
//                }
//
//
//                override fun onFailure(call: Call<RegisterListResponse>, t: Throwable) {
//                    progressBarregister.visibility = View.GONE
//                    Log.e(ContentValues.TAG, t.toString())
//                }
//            })
//
//
//        } else {
//            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
//
//        }
//
//    }

}