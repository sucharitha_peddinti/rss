package com.erepairs.app.api


data class DefaultResponse(val error: Boolean, val message: String, val response: String)

