package com.erepairs.app.api

import com.erepairs.app.models.LoginListResponse
import com.erepairs.app.models.RegisterListResponse
import com.rss.royalit.model.NotesListResponse
import com.rss.royalit.model.NotificationssListResponse
import com.supernal.royalsecuritysystems.models.*
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST


interface Api {
    @FormUrlEncoded
    @POST("client_resistration")
    fun postregister(
        @Field("api_key") api_key: String,
        @Field("company") company: String,
        @Field("fullname") fullname: String,
        @Field("email") email: String,
        @Field("mobile") mobile: String,
        @Field("gstin") gstin: String,
        @Field("address") address: String
    ): Call<LoginDefaultResponse>

    @FormUrlEncoded
    @POST("client_login")
    fun postlogin(
        @Field("api_key") api_key: String,

        @Field("mobile") mobile: String

    ): Call<LoginListResponse>

    @FormUrlEncoded
    @POST("client_otp_verify")
    fun postotp(
        @Field("api_key") api_key: String,
        @Field("mobile") mobile_num: String,
        @Field("login_otp") login_otp: String,
        @Field("device_token") device_token: String

    ): Call<LoginListResponse>


    @FormUrlEncoded
    @POST("services_list")
    fun getServicesList(
        @Field("api_key") api_key: String

    ): Call<ServiceResponse>


    @FormUrlEncoded
    @POST("get_notifications_data")
    fun notification_msgs_data(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String

    ): Call<NotificationssListResponse>


    @FormUrlEncoded
    @POST("client_all_important_notes")
    fun client_all_important_notes(
        @Field("api_key") api_key: String
    ): Call<NotesListResponse>


    @FormUrlEncoded
    @POST("client_contactus")
    fun contactus_api(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String,
        @Field("fullname") fullname: String,
        @Field("email") email: String,
        @Field("mobile") mobile: String,
        @Field("description") description: String

    ): Call<LoginDefaultResponse>

    @FormUrlEncoded
    @POST("client_update_profile")
    fun updateProfileDetails(
        @Field("api_key") api_key: String,
        @Field("name") fname: String,
        @Field("company") cname: String,
        @Field("gstin") gstinno: String,
        @Field("address") address: String,
        @Field("website") website: String,
        @Field("client_id") client_id: String

    ): Call<ProfileResponse>

    @FormUrlEncoded
    @POST("get_profile_details")
    fun getProfileDetails(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String

    ): Call<ProfileResponse>

    @FormUrlEncoded
    @POST("services_details_list")
    fun servicesdetailsList(
        @Field("api_key") api_key: String,
        @Field("service_id") service_id: String

    ): Call<ServiceDetailsResponse>

    @FormUrlEncoded
    @POST("brands_list")
    fun getBrandsList(
        @Field("api_key") api_key: String

    ): Call<BrandsResponse>


    @FormUrlEncoded
    @POST("client_active_tickets")
    fun active_tickets(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String

    ): Call<ActiveTicketsResponse>

    @FormUrlEncoded
    @POST("client_projects")
    fun client_projects(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String

    ): Call<ProjectResponse>

    @FormUrlEncoded
    @POST("client_closed_tickets")
    fun closed_tickets(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String

    ): Call<ActiveTicketsResponse>

    @FormUrlEncoded
    @POST("whats_new_data")
    fun get_whatsnewdata(
        @Field("api_key") api_key: String
    ): Call<WhatsnewResponse>

    @FormUrlEncoded
    @POST("client_alltypes_invoices")
    fun alltypes_invoices(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String,
        @Field("invoice_type") invoice_type: String

    ): Call<InvoiceResponse>

    @FormUrlEncoded
    @POST("ticket_comments_save")
    fun invoice_comments_save(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String,
        @Field("ticket_id") ticket_id: String,
        @Field("comments") comments: String

    ): Call<ChatResponse>

    @FormUrlEncoded
    @POST("client_ticket_comments")
    fun client_invoice_comments(
        @Field("api_key") api_key: String,
        @Field("ticket_id") ticket_id: String
    ): Call<ChatResponse>

    @FormUrlEncoded
    @POST("client_service_request")
    fun service_request(
        @Field("api_key") api_key: String,
        @Field("client_id") client_id: String,
        @Field("name") name: String? = null,
        @Field("cmpny_name") cmpny_name: String,
        @Field("email") email: String,
        @Field("mobile") mobile: String,
        @Field("product_details") product_details: String,
        @Field("complaint_details") complaint_details: String

    ): Call<LoginDefaultResponse>

    @FormUrlEncoded
    @POST("notification_msgs_data")
    fun notification_msgs_data(
        @Field("api_key") api_key: String

    ): Call<NotificationssListResponse>

    @FormUrlEncoded
    @POST("client_important_notes")
    fun client_important_notes(
        @Field("api_key") api_key: String

    ): Call<ClientResponse>

}
