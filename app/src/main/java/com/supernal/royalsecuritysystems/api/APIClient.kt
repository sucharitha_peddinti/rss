package com.erepairs.app.api

import com.google.gson.GsonBuilder
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type

/**
 * Created by sucharitha  on 6/08/2021.
 */
class APIClient {

    companion object {
        private const val BASE_URL = "https://projects.royalcctv.com/Api/"
        private const val API_KEY = "f291f0e58e385d9adf7213884783cgfg54ae"


        var retofit: Retrofit? = null

        val client: Retrofit
            get() {

                if (retofit == null) {
                    val gson = GsonBuilder()

                    retofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
//                        .addConverterFactory(NullOnEmptyConverterFactory())

                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                }
                return retofit!!
            }
    }

    internal class NullOnEmptyConverterFactory : Converter.Factory() {
        fun responseBody(
            type: Type?,
            annotations: Array<Annotation?>?,
            retrofit: Retrofit
        ): Any {
            val delegate: Converter<ResponseBody, *> =
                retrofit.nextResponseBodyConverter<Any>(this, type, annotations)
            return object {
                fun convert(body: ResponseBody): Any? {
                    return if (body.contentLength() == 0L) null else delegate.convert(body)
                }
            }
        }
    }

}

