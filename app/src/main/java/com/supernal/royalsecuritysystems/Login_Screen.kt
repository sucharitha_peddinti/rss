package com.supernal.royalsecuritysystems

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.models.LoginListResponse
import com.erepairs.app.utils.NetWorkConection
import com.google.gson.JsonSyntaxException
import com.supernal.royalsecuritysystems.databinding.LoginScreenBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login_Screen : Activity() {

    private lateinit var binding: LoginScreenBinding
    lateinit var sharedPreferences: SharedPreferences

    lateinit var mobile_signin_edit: EditText
    lateinit var mobile_stng: String
    lateinit var login_btn: Button
    lateinit var progressBarlogin: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginScreenBinding.inflate(layoutInflater)

        setContentView(binding.root)

        sharedPreferences = getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

        mobile_signin_edit = binding.mobileSignin
        login_btn = binding.loginBtn
        progressBarlogin = binding.progressBarlogin


        binding.loginBtn.setOnClickListener {
            mobile_stng = mobile_signin_edit.text.toString()

            if (mobile_stng.isEmpty()) {
                mobile_signin_edit.error = "Please Enter Mobile Number"
            } else {
                postLogin()

            }

        }
        binding.createacc.setOnClickListener {

            val intent = Intent(this, Signup_Screen::class.java)
            startActivity(intent)
            finish()
        }

        binding.forgotBtn.setOnClickListener {

            val intent = Intent(this, ForgotPwd_Screen::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun postLogin() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            //Set the Adapter to the RecyclerView//

            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.postlogin(
                    getString(R.string.api_key),
                    mobile_stng
                )
            Log.e("mobile_stng", mobile_stng)
            progressBarlogin.visibility = View.VISIBLE

            call.enqueue(object : Callback<LoginListResponse> {
                override fun onResponse(
                    call: Call<LoginListResponse>,
                    response: Response<LoginListResponse>
                ) {
                    progressBarlogin.visibility = View.GONE

                    Log.e(ContentValues.TAG, response.body()!!.message)

                    val status_code = response.body()?.code
                    val status = response.body()?.status
                    if ((status!!.equals(true) || (status == true))) {
                        if ((status_code!!.equals(1) || (status_code == 1))) {

                            //Set the Adapter to the RecyclerView//

                            try {


                                val listOfcategories = response.body()?.response

                                Log.e("res", "" + listOfcategories)
                                val editor = sharedPreferences.edit()
                                editor.putString("mobile_number", mobile_stng)
                                editor.commit()
//                                Toast.makeText(
//                                    this@Login_Screen,
//                                    "" + response.body()?.message,
//                                    Toast.LENGTH_LONG
//                                ).show()

                                Log.e("mobile", response.body()!!.response.mobile)
                                val intent = Intent(this@Login_Screen, Otp_Screen::class.java)
                                startActivity(intent)
                                finish()

                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                            } catch (e: TypeCastException) {
                                e.printStackTrace()
                            } catch (e: JsonSyntaxException) {
                                e.printStackTrace()
                            } catch (
                                e: IllegalStateException
                            ) {
                                e.printStackTrace()
                            }

                        }
                    } else if ((status_code!!.equals(2) || (status_code == 2))) {
                        Toast.makeText(
                            this@Login_Screen,
                            "" + response.body()?.message,
                            Toast.LENGTH_LONG
                        ).show()

                    } else if ((status.equals(false) || (status == false))) {
                        Toast.makeText(
                            this@Login_Screen,
                            "" + response.body()?.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }


                }

                override fun onFailure(call: Call<LoginListResponse>, t: Throwable) {
                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, call.toString())
                    Toast.makeText(
                        this@Login_Screen,
                        "Invalid Mobile Number",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }

}