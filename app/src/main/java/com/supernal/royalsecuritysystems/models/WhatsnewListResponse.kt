package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class WhatsnewListResponse(


    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("enquiry_id") val enquiry_id: String,
    @SerializedName("content") val content: String,
    @SerializedName("image") val image: String,
    @SerializedName("status") val status: String,
    @SerializedName("updated_date") val updated_date: String,
    @SerializedName("created_date") val created_date: String
)