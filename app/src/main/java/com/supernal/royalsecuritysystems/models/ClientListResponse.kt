package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ClientListResponse (

    @SerializedName("id"           ) var id          : String? = null,
    @SerializedName("title"        ) var title       : String? = null,
    @SerializedName("description"  ) var description : String? = null,
    @SerializedName("created_date" ) var createdDate : String? = null
)