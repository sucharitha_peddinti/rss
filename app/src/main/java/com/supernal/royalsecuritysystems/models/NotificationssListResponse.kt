package com.rss.royalit.model

import com.google.gson.annotations.SerializedName
import com.supernal.royalsecuritysystems.models.NotificationssResponse

/**
 *  Created by Sucharitha Peddinti on 11/10/21.
 */
data class NotificationssListResponse(

    @SerializedName("Status") val status : Boolean,
    @SerializedName("Message") val message : String,
    @SerializedName("Response") val response : List<NotificationssResponse>,
    @SerializedName("code") val code : Int
)