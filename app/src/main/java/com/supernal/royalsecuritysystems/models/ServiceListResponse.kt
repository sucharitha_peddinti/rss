package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ServiceListResponse(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("content") val content: String,
    @SerializedName("image") val image: String,
    @SerializedName("is_active") val is_active: Int,
    @SerializedName("created_date") val created_date: String
)