package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class BrandsResponse (

    @SerializedName("Status") val status : Boolean,
    @SerializedName("Message") val message : String,
    @SerializedName("Response") val response : List<BrandsListResponse>,
    @SerializedName("code") val code : Int
)