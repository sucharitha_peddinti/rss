package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ActiveTicketsResponse (

    @SerializedName("Status") val status : Boolean,
    @SerializedName("Message") val message : String,
    @SerializedName("Response") val response : List<ActiveTicketsListResponse>,
    @SerializedName("code") val code : Int
)