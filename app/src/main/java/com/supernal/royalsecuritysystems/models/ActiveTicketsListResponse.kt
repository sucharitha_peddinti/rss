package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ActiveTicketsListResponse(


    @SerializedName("id") val id: Int,
    @SerializedName("client_id") val client_id: Int,
    @SerializedName("enquiry_id") val enquiry_id: String,
    @SerializedName("name") val name: String,
    @SerializedName("cmpny_name") val cmpny_name: String,
    @SerializedName("email") val email: String,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("product_details") val product_details: String,
    @SerializedName("complaint_details") val complaint_details: String,
    @SerializedName("created_date") val created_date: String,
    @SerializedName("closed_person") val closed_person: String?,
    @SerializedName("complaint_details_status") val complaint_details_status: String?,
    @SerializedName("amount") val amount: String?,
    @SerializedName("product_warrenty") val product_warrenty: String?,
    @SerializedName("product_warrenty_details") val product_warrenty_details: String?,
    @SerializedName("product_replace") val product_replace: String?,
    @SerializedName("product_replace_details") val product_replace_details: String?
)