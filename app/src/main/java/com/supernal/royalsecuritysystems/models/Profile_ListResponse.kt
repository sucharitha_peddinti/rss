package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class Profile_ListResponse(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("gstin") val gstin: String
)