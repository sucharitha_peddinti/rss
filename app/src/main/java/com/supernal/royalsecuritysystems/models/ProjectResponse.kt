package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ProjectResponse (

    @SerializedName("Status") val status : Boolean,
    @SerializedName("Message") val message : String,
    @SerializedName("Response") val response : List<Project_ListResponse>,
    @SerializedName("code") val code : Int
)