package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class Project_ListResponse(
    @SerializedName("id") val id : Int,
    @SerializedName("client_id") val client_id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("title") val title : String,
    @SerializedName("created_date") val created_date : String,
    @SerializedName("status") val status : String,
    @SerializedName("pc_id") val pc_id : Int,
    @SerializedName("pcstatus") val pcstatus : Int,
    @SerializedName("warrenty_days") val warrenty_days : Int
)