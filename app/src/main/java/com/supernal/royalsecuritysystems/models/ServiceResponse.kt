package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ServiceResponse (

    @SerializedName("Status") val status : Boolean,
    @SerializedName("Message") val message : String,
    @SerializedName("Response") val response : List<ServiceListResponse>,
    @SerializedName("code") val code : Int
)