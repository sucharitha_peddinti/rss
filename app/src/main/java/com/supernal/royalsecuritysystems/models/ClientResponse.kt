package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ClientResponse (

    @SerializedName("Status"   ) var Status   : Boolean?  = null,
    @SerializedName("Message"  ) var Message  : String?   = null,
    @SerializedName("Response" ) var Response : ClientListResponse? = ClientListResponse(),
    @SerializedName("code"     ) var code     : Int?      = null
)