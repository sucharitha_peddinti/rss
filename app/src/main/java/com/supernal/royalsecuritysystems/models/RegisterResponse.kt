package com.erepairs.app.models

import com.google.gson.annotations.SerializedName

data class RegisterResponse(


    @SerializedName("company") val company: String,
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("created_date") val created_date: String,
    @SerializedName("user_id") val user_id: Int
)