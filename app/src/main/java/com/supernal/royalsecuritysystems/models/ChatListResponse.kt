package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ChatListResponse(

    @SerializedName("id") val id: Int,
    @SerializedName("comment_from") val comment_from: String,
    @SerializedName("invoice_id") val invoice_id: Int,
    @SerializedName("commenter_id") val commenter_id: Int,
    @SerializedName("comments") val comments: String,
    @SerializedName("created_date") val created_date: String,
    @SerializedName("person_name") val person_name: String,
    @SerializedName("person_photo") val person_photo: String
)