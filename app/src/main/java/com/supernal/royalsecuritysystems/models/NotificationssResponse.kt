package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

/**
 *  Created by Sucharitha Peddinti on 11/10/21.
 */
data class NotificationssResponse(

    @SerializedName("title") val title: String,
    @SerializedName("body") val body: String,
    @SerializedName("created_date") val created_date: String,
    @SerializedName("id") val id: Int
)