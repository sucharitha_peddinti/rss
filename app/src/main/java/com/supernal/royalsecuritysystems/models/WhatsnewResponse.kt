package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class WhatsnewResponse (

    @SerializedName("Status") val status : Boolean,
    @SerializedName("Message") val message : String,
    @SerializedName("Response") val response : List<WhatsnewListResponse>,
    @SerializedName("code") val code : Int
)