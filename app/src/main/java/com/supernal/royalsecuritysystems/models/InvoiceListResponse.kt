package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class InvoiceListResponse(


    @SerializedName("id") val id: Int,
    @SerializedName("invoice_number") val invoice_number: String,
    @SerializedName("client_id") val client_id: Int,
    @SerializedName("cgst") val cgst: Int,
    @SerializedName("sgst") val sgst: Int,
    @SerializedName("discount") val discount: Int,
    @SerializedName("cgst_amount") val cgst_amount: Double,
    @SerializedName("sgst_amount") val sgst_amount: Double,
    @SerializedName("total_amount") val total_amount: Double,
    @SerializedName("discount_amount") val discount_amount: Int,
    @SerializedName("amount_paid") val amount_paid: Double,
    @SerializedName("invoice_date") val invoice_date: String,
    @SerializedName("due_date") val due_date: String,
    @SerializedName("paystatus") val paystatus: Int,
    @SerializedName("pstatus") val pstatus: Int,
    @SerializedName("project_type") val project_type: Int,
    @SerializedName("gst_type") val gst_type: Int,
    @SerializedName("is_active") val is_active: Int,
    @SerializedName("ip") val ip: String,
    @SerializedName("created_date") val created_date: String,
    @SerializedName("created_by") val created_by: Int,
    @SerializedName("updated_date") val updated_date: String,
    @SerializedName("updated_by") val updated_by: Int,
    @SerializedName("gstin") val gstin: String,
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("status") val status: String,
    @SerializedName("fname") val fname: String,
    @SerializedName("lname") val lname: String,
    @SerializedName("download_url") val download_url: String,
    @SerializedName("warrenty_days") val warrenty_days: Int
)