package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class LoginDefaultResponse(

    @SerializedName("Status") val status: Boolean,
    @SerializedName("Message") val message: String,
    @SerializedName("Response") val response:String,
    @SerializedName("code") val code: Int
)