package com.erepairs.app.models

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("type") val type: Int,
    @SerializedName("service") val service: Int,
    @SerializedName("company") val company: String,
    @SerializedName("website") val website: String,
    @SerializedName("city") val city: String,
    @SerializedName("state") val state: String,
    @SerializedName("country") val country: String,
    @SerializedName("zipcode") val zipcode: String,
    @SerializedName("address") val address: String,
    @SerializedName("gstin") val gstin: String,
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("amobile") val amobile: String,
    @SerializedName("dob") val dob: String,
    @SerializedName("otp") val otp: String,
    @SerializedName("ip") val ip: String,
    @SerializedName("agree") val agree: Int,
    @SerializedName("agree1") val agree1: Int,
    @SerializedName("is_active") val is_active: Int,
    @SerializedName("status") val status: Int,
    @SerializedName("login_otp") val login_otp: String = "",
    @SerializedName("created_by") val created_by: Int,
    @SerializedName("created_date") val created_date: String,
    @SerializedName("updated_by") val updated_by: Int,
    @SerializedName("updated_date") val updated_date: String
)