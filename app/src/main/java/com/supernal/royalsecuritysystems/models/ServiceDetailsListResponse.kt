package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class ServiceDetailsListResponse(


    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("image") val image : String,
    @SerializedName("content") val content : String,
    @SerializedName("status") val status : Int,
    @SerializedName("updated_date") val updated_date : String
)