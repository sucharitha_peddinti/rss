package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

data class BrandsListResponse(
    @SerializedName("id") val id : Int,
    @SerializedName("brand") val brand : String,
    @SerializedName("service_id") val service_id : Int,
    @SerializedName("is_active") val is_active : Int,
    @SerializedName("created_date") val created_date : String,
    @SerializedName("image") val image : String
)