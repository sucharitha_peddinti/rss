package com.supernal.royalsecuritysystems.models

import com.google.gson.annotations.SerializedName

/**
 *  Created by Sucharitha Peddinti on 11/10/21.
 */
data class NotessResponse(

    @SerializedName("id"           ) var id          : String? = null,
    @SerializedName("title"        ) var title       : String? = null,
    @SerializedName("description"  ) var description : String? = null,
    @SerializedName("created_date" ) var createdDate : String? = null
)