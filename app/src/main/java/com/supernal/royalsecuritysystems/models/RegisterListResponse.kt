package com.erepairs.app.models

import com.google.gson.annotations.SerializedName

data class RegisterListResponse(

    @SerializedName("Status") val status : Boolean,
    @SerializedName("Message") val message : String,
    @SerializedName("Response") val response : RegisterResponse?,
    @SerializedName("code") val code : Int
)