package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import android.webkit.URLUtil
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.InvoiceListResponse

class PaidInvoice_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<InvoiceListResponse>
) :
    RecyclerView.Adapter<PaidInvoice_Adapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PaidInvoice_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.invoice_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: PaidInvoice_Adapter.ViewHolder, position: Int) {


        holder.invoice_id.text = userList.get(position).invoice_number
        holder.in_name.text = userList.get(position).name.toString()
        holder.in_amount.text = "\u20B9" + userList.get(position).total_amount.toString()
        holder.in_warranty.text = userList.get(position).warrenty_days.toString() + " Days"

        holder.invoice_id.setOnClickListener {

//
//            val navController =
//                Navigation.findNavController(
//                    context as Activity,
//                    R.id.nav_host_fragment_content_home
//                )
//            navController.navigate(R.id.navigation_invoicewebview)


            val builder = AlertDialog.Builder(context)
            //set title for alert dialog
            builder.setTitle("Download Invoice")
            //set message for alert dialog
            builder.setMessage("Do you want to download Invoce PDF?")

            holder.webview.loadUrl(userList.get(position).download_url)
            holder.webview.webViewClient = MyClient()
            //performing positive action
            builder.setPositiveButton("Yes") { dialogInterface, which ->
                holder.webview.setDownloadListener { url, userAgent, contentDisposition, mimeType, contentLength ->
                    val request = DownloadManager.Request(Uri.parse(url))
                    request.setMimeType(mimeType)
                    request.addRequestHeader("cookie", CookieManager.getInstance().getCookie(url))
                    request.addRequestHeader("User-Agent", userAgent)
                    request.setDescription("Downloading file...")
                    request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType))
                    request.allowScanningByMediaScanner()
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    request.setDestinationInExternalFilesDir(
                        context,
                        Environment.DIRECTORY_DOWNLOADS,
                        ".pdf"
                    )
                    val dm = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                    dm.enqueue(request)
                    Toast.makeText(context, "Downloading File", Toast.LENGTH_LONG).show()
                }

            }

            //performing negative action
            builder.setNegativeButton("No") { dialogInterface, which ->

                builder.setCancelable(true)
            }
            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()


        }


    }


    class MyClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, Url: String): Boolean {
            view.loadUrl(Url)
            return true

        }
    }


    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val invoice_id = itemView.findViewById(R.id.invoice_id) as TextView
        val in_name = itemView.findViewById(R.id.in_name) as TextView
        val in_amount = itemView.findViewById(R.id.in_amount) as TextView
        val in_warranty = itemView.findViewById(R.id.in_warranty) as TextView
        val webview = itemView.findViewById(R.id.webview) as WebView

    }

    override fun getItemCount(): Int {

        return userList.size
    }
}

