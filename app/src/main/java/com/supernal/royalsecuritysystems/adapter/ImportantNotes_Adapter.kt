package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.NotessResponse

class ImportantNotes_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<NotessResponse>,
) : RecyclerView.Adapter<ImportantNotes_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): ImportantNotes_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.importantnotes_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: ImportantNotes_Adapter.ViewHolder, position: Int) {


        //add  following code in onBindViewHolder
//        if (position % 2 == 1) {
//            holder.linear_notifi.setBackgroundColor(context.resources.getColor(R.color.white))
//        } else {
//            holder.linear_notifi.setBackgroundColor(context.resources.getColor(R.color.grey200))
//        }
        holder.title_notifi.text = "" + userList.get(position).description
        holder.body_notifi.text = "" + userList.get(position).title.toString()
        try {

            holder.date_notifi.text = "" + userList.get(position).createdDate

        } catch (e: NullPointerException) {
            e.printStackTrace()
        }


        holder.linear_notes.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.adslayout, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context,R.style.CustomDialog)
                .setView(mDialogView)
            //show dialog
            val mAlertDialog = mBuilder.show()
            //login button click of custom layout

            val title_TV = mDialogView.findViewById(R.id.title_TV) as TextView
            val closeIV = mDialogView.findViewById(R.id.closeIV) as ImageView
            val desctitle_TV = mDialogView.findViewById(R.id.desctitle_TV) as TextView
            closeIV.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout

            }

            title_TV.text = "" + userList.get(position).title
            desctitle_TV.text = "" + userList.get(position).description
        }


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title_notifi = itemView.findViewById(R.id.content_notes) as TextView
        val body_notifi = itemView.findViewById(R.id.title_notes) as TextView
        val date_notifi = itemView.findViewById(R.id.date_notes) as TextView
        val linear_notes = itemView.findViewById(R.id.linear_notes) as LinearLayout


    }
}
