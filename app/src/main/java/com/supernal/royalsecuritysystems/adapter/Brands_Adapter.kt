package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.BrandsListResponse

class Brands_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<BrandsListResponse>
) :
    RecyclerView.Adapter<Brands_Adapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Brands_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.homebrands_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: Brands_Adapter.ViewHolder, position: Int) {


        holder.brandname_text.text = userList.get(position).brand
        Glide.with(context).load(userList.get(position).image)
            .apply(RequestOptions().centerCrop())
            .transform(CenterCrop(), RoundedCorners(20))

            .error(R.drawable.logo)
            .into(holder.brand_image)


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val brand_image = itemView.findViewById(R.id.brand_image) as ImageView
        val brandname_text = itemView.findViewById(R.id.brandname_text) as TextView


    }
}
