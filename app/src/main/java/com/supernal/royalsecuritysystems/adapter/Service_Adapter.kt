package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.ServiceListResponse

class Services_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<ServiceListResponse>
) :
    RecyclerView.Adapter<Services_Adapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Services_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.service_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: Services_Adapter.ViewHolder, position: Int) {


        holder.text_service.text = userList.get(position).name


        Glide.with(context)
            .load(userList.get(position).image)
            .placeholder(R.drawable.logo)
            .into(holder.image_service)


        holder.servicedet_linear.setOnClickListener {

            val sharedPreferences =
                context.getSharedPreferences(
                    "loginprefs",
                    Context.MODE_PRIVATE
                )

            val editor = sharedPreferences.edit()
            editor.putString("service_id", userList.get(position).id.toString())
            editor.commit()
            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home)
            navController.navigate(R.id.navigation_servicesdetails)


        }


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val image_service = itemView.findViewById(R.id.image_service) as ImageView
        val text_service = itemView.findViewById(R.id.service_textt) as TextView
        val servicedet_linear = itemView.findViewById(R.id.servicedet_linear) as LinearLayout


    }
}
