package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.CookieManager
import android.webkit.URLUtil
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.InvoiceListResponse

class NotPaidInvoice_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<InvoiceListResponse>
) :
    RecyclerView.Adapter<NotPaidInvoice_Adapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotPaidInvoice_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.notpaidinvoice_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: NotPaidInvoice_Adapter.ViewHolder, position: Int) {


        holder.notp_invoice_id.text = userList.get(position).invoice_number
        holder.notp_in_name.text = userList.get(position).name.toString()
        holder.notp_in_amount.text = "\u20B9" + userList.get(position).total_amount.toString()
        holder.notp_warranty.text = userList.get(position).warrenty_days.toString() + " Days"

        holder.notp_invoice_id.setOnClickListener {

            val builder = AlertDialog.Builder(context)
            //set title for alert dialog
            builder.setTitle("Download Invoice")
            //set message for alert dialog
            builder.setMessage("Do you want to download Invoce PDF?")

            holder.webview.loadUrl(userList.get(position).download_url)
            holder.webview.webViewClient = PaidInvoice_Adapter.MyClient()
            //performing positive action
            builder.setPositiveButton("Yes") { dialogInterface, which ->
                holder.webview.setDownloadListener { url, userAgent, contentDisposition, mimeType, contentLength ->
                    val request = DownloadManager.Request(Uri.parse(url))
                    request.setMimeType(mimeType)
                    request.addRequestHeader("cookie", CookieManager.getInstance().getCookie(url))
                    request.addRequestHeader("User-Agent", userAgent)
                    request.setDescription("Downloading file...")
                    request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType))
                    request.allowScanningByMediaScanner()
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    request.setDestinationInExternalFilesDir(
                        context,
                        Environment.DIRECTORY_DOWNLOADS,
                        ".pdf"
                    )
                    val dm = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                    dm.enqueue(request)
                    Toast.makeText(context, "Downloading File", Toast.LENGTH_LONG).show()
                }

            }

            //performing negative action
            builder.setNegativeButton("No") { dialogInterface, which ->


            }
            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()


        }
        holder.paynow_text.setOnClickListener {

            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.bankdetails_screen)
            val body = dialog.findViewById(R.id.dialogupdateBtn) as Button

            body.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    class MyClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, Url: String): Boolean {
            view.loadUrl(Url)
            return true

        }
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val notp_invoice_id = itemView.findViewById(R.id.notp_invoice_id) as TextView
        val notp_in_name = itemView.findViewById(R.id.notp_in_name) as TextView
        val notp_in_amount = itemView.findViewById(R.id.notp_in_amount) as TextView
        val notp_warranty = itemView.findViewById(R.id.notp_warranty) as TextView
        val paynow_text = itemView.findViewById(R.id.paynow_text) as TextView
        val webview = itemView.findViewById(R.id.webview) as WebView


    }
}
