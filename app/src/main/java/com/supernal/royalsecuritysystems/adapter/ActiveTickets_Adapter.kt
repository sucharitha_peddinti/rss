package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.ActiveTicketsListResponse

class ActiveTickets_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<ActiveTicketsListResponse>
) :
    RecyclerView.Adapter<ActiveTickets_Adapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActiveTickets_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.activetickets_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: ActiveTickets_Adapter.ViewHolder, position: Int) {


        holder.tickesummary.text = userList.get(position).complaint_details
        holder.tickes_id.text = userList.get(position).enquiry_id.toString()


        holder.open_btn.setOnClickListener {
            val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.tickets_view, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
            //show dialog
            val mAlertDialog = mBuilder.show()
            //login button click of custom layout

            val body = mDialogView.findViewById(R.id.ticketid_text) as TextView
            val ticketdesc_text = mDialogView.findViewById(R.id.ticketdesc_text) as TextView
            val close_btn = mDialogView.findViewById(R.id.close_btn) as ImageView

            body.text = "TICKET NUMBER : " + userList.get(position).enquiry_id
            ticketdesc_text.text = "" + userList.get(position).complaint_details
            close_btn.setOnClickListener {
                mAlertDialog.dismiss()
            }
            //cancel button click of custom layout


        }
        holder.chat_icon.setOnClickListener {

            val sharedPreferences =
                context.getSharedPreferences(
                    "loginprefs",
                    Context.MODE_PRIVATE
                )

            val editor = sharedPreferences.edit()
            editor.putString("ticket_id", userList.get(position).id.toString())
            editor.commit()
            Log.e("chat adapter ticket_id", userList.get(position).id.toString())

            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_chat)


        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tickes_id = itemView.findViewById(R.id.tickes_id) as TextView
        val tickesummary = itemView.findViewById(R.id.tickesummary) as TextView
        val chat_icon = itemView.findViewById(R.id.chat_icon) as ImageView
        val open_btn = itemView.findViewById(R.id.open_btn) as TextView


    }
}
