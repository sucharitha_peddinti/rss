package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.ActiveTicketsListResponse

class ClosedTickets_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<ActiveTicketsListResponse>
) : RecyclerView.Adapter<ClosedTickets_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClosedTickets_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.closedtickets_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: ClosedTickets_Adapter.ViewHolder, position: Int) {


        holder.tickesummary.text = userList.get(position).complaint_details
        holder.tickes_id.text = userList.get(position).enquiry_id.toString()
        holder.chat_icon.setOnClickListener {

            val sharedPreferences =
                context.getSharedPreferences(
                    "loginprefs",
                    Context.MODE_PRIVATE
                )

            val editor = sharedPreferences.edit()
            editor.putString("ticket_id", userList.get(position).id.toString())
            editor.commit()

            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_c_chat)

        }

        holder.eye_icon.setOnClickListener {
            val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.closedticketsvieww_, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
            //show dialog
            val mAlertDialog = mBuilder.show()
            //login button click of custom layoutf

            val employeename = mDialogView.findViewById(R.id.employeename) as TextView
            val complaintdetails_text =
                mDialogView.findViewById(R.id.complaintdetails_text) as TextView
            val amount_text = mDialogView.findViewById(R.id.amount_text) as TextView
            val warrenty_text = mDialogView.findViewById(R.id.warrenty_text) as TextView
            val productitem_text = mDialogView.findViewById(R.id.productitem_text) as TextView
            val close_btn = mDialogView.findViewById(R.id.close_btn) as ImageView
            val amount_li = mDialogView.findViewById(R.id.amount_li) as LinearLayout
            val prodcut_li = mDialogView.findViewById(R.id.prodcut_li) as LinearLayout
            val emp_line = mDialogView.findViewById(R.id.emp_line) as LinearLayout

            productitem_text.text =
                userList.get(position).product_replace + "\n" + userList.get(position).product_replace_details
            complaintdetails_text.text = "" + userList.get(position).complaint_details_status

            if (userList.get(position).closed_person.equals(null) || (userList.get(position).closed_person!!.isEmpty())) {
                employeename.visibility = View.GONE
            } else {
                employeename.visibility = View.VISIBLE

                employeename.text = "" + userList.get(position).closed_person

            }
            if (userList.get(position).amount.equals(null) || (userList.get(position).amount!!.isEmpty())) {
                amount_text.visibility = View.GONE
            } else {
                amount_text.visibility = View.VISIBLE

                amount_text.text = "" + userList.get(position).amount

            }
            if (userList.get(position).product_warrenty.equals(null) || (userList.get(position).product_warrenty!!.isEmpty())) {
                warrenty_text.visibility = View.GONE
            } else {
                warrenty_text.visibility = View.VISIBLE

                warrenty_text.text =
                    "" + userList.get(position).product_warrenty + "\n" + userList.get(position).product_warrenty_details

            }




            close_btn.setOnClickListener {
                mAlertDialog.dismiss()
            }
            //cancel button click of custom layout


        }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tickes_id = itemView.findViewById(R.id.close_tickes_id) as TextView
        val tickesummary = itemView.findViewById(R.id.close_tickesummary) as TextView
        val chat_icon = itemView.findViewById(R.id.chat_icon) as ImageView
        val eye_icon = itemView.findViewById(R.id.eye_icon) as ImageView


    }
}
