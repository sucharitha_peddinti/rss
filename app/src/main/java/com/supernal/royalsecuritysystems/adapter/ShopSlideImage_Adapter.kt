package com.supernal.royalsecuritysystems.adapter

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.WhatsnewListResponse

/**
 * Created by suchipeddinti on 10/07/2019.
 */
class ShopSlideImage_Adapter(
    private val context: Context,
    private val imageModelArrayList: ArrayList<WhatsnewListResponse>
) : PagerAdapter() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.home_adapter, view, false)!!

        val imageView = imageLayout.findViewById(R.id.shopsliderimage) as ImageView
        val title_whats = imageLayout.findViewById(R.id.title_whats) as TextView
        val title_desc = imageLayout.findViewById(R.id.title_desc) as TextView

//        imageView.setBackgroundResource(imageModelArrayList.get(position).getImage_drawables())

        Glide.with(context).load(imageModelArrayList.get(position).image)
            .apply(RequestOptions().centerCrop())
            .transform(CenterCrop(), RoundedCorners(30))

            .error(R.drawable.logo)
            .into(imageView)
        view.addView(imageLayout, 0)

        title_whats.text = imageModelArrayList.get(position).title
        title_desc.text = imageModelArrayList.get(position).content

        if (imageModelArrayList[position].image == "" || (imageModelArrayList[position].image.equals(
                ""
            ))
        ) {
            title_desc.visibility = View.VISIBLE
            imageView.visibility = View.GONE


        } else {
            title_desc.visibility = View.GONE
            imageView.visibility = View.VISIBLE


        }
//
//        if ((imageModelArrayList[position].content == "") || (imageModelArrayList[position].image.equals(
//                ""
//            ))
//        ) {
//            title_desc.visibility = View.GONE
//            imageView.visibility = View.VISIBLE
//
//        } else {
//            title_desc.visibility = View.VISIBLE
//            imageView.visibility = View.GONE
//
//
//        }


        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }


}