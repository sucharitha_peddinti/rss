package com.supernal.royalsecuritysystems.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.supernal.royalsecuritysystems.R;
import com.supernal.royalsecuritysystems.models.ChatListResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MessageChat_Adapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private final Context mContext;
    private final List<ChatListResponse> mMessageList;

    public MessageChat_Adapter(Context context, List<ChatListResponse> messageList) {
        mContext = context;
        mMessageList = messageList;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        ChatListResponse message = mMessageList.get(position);


        if (message.getComment_from().equals("client")) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;

        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_receive_adater, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_adater, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatListResponse message = mMessageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_gchat_message_me);
            timeText = itemView.findViewById(R.id.text_gchat_date_me);
        }

        void bind(ChatListResponse message) {
            try {
                messageText.setText(message.getComments());

                // Format the stored timestamp into a readable String using method.
                String sampledate = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat sdf = new SimpleDateFormat(sampledate);
                Date todayWithZeroTime = sdf.parse(message.getCreated_date());

                SimpleDateFormat  timeformat= new SimpleDateFormat("hh:mm a");
                SimpleDateFormat  dateFormat= new SimpleDateFormat("yyy-MM-dd");

                String time = timeformat.format(todayWithZeroTime);
                String date = dateFormat.format(todayWithZeroTime);
                // Format the stored timestamp into a readable String using method.
                timeText.setText("" + date + " " + time);

                Log.e("time", message.getCreated_date());
            } catch (NullPointerException | ParseException e) {
                e.printStackTrace();
            }

        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_gchat_message_other);
            timeText = itemView.findViewById(R.id.text_gchat_date_other);
            nameText = itemView.findViewById(R.id.text_gchat_user_other);
        }

        void bind(ChatListResponse message) {
            try {


                messageText.setText(message.getComments());


                String sampledate = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat sdf = new SimpleDateFormat(sampledate);
                Date todayWithZeroTime = sdf.parse(message.getCreated_date());

                SimpleDateFormat  timeformat= new SimpleDateFormat("hh:mm a");
                SimpleDateFormat  dateFormat= new SimpleDateFormat("yyy-MM-dd");

                String time = timeformat.format(todayWithZeroTime);
                String date = dateFormat.format(todayWithZeroTime);
                // Format the stored timestamp into a readable String using method.
                timeText.setText("" + date + " " + time);

                Log.e("time", message.getCreated_date());
                nameText.setText(message.getComment_from() + " Says... ");
            } catch (NullPointerException | ParseException e) {
                e.printStackTrace();
            }
            // Insert the profile image from the URL into the ImageView.
        }
    }
}
