package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.models.NotificationssResponse
import com.supernal.royalsecuritysystems.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class Notifications_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<NotificationssResponse>
) : RecyclerView.Adapter<Notifications_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Notifications_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.notification_adapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: Notifications_Adapter.ViewHolder, position: Int) {


        //add  following code in onBindViewHolder
//        if (position % 2 == 1) {
//            holder.linear_notifi.setBackgroundColor(context.resources.getColor(R.color.white))
//        } else {
//            holder.linear_notifi.setBackgroundColor(context.resources.getColor(R.color.grey200))
//        }
        holder.title_notifi.text =""+ userList.get(position).body
        holder.body_notifi.text ="" +userList.get(position).title.toString()
        try {


            val fmt = "yyyy-MM-dd HH:mm:ss"

            val df: DateFormat = SimpleDateFormat(fmt)

            val dt: Date = df.parse(userList.get(position).created_date)

            val tdf: DateFormat = SimpleDateFormat("HH MMM")
            val dfmt: DateFormat = SimpleDateFormat("yyyy-MM-dd")


            val timeOnly: String = tdf.format(dt)
            val dateOnly: String = dfmt.format(dt)
            System.out.println(timeOnly)

            Log.e("date", "" + dateOnly)
            Log.e("time", "" + timeOnly)
            holder.date_notifi.text = "" + timeOnly

        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        Log.e("id", "" + userList.get(position).id)
        Log.e("title", "" + userList.get(position).body)
        Log.e("body", "" + userList.get(position).title)

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title_notifi = itemView.findViewById(R.id.content_notifi) as TextView
        val body_notifi = itemView.findViewById(R.id.title_notifi) as TextView
        val date_notifi = itemView.findViewById(R.id.date_notifi) as TextView
        val linear_notifi = itemView.findViewById(R.id.linear_notifi) as LinearLayout


    }
}
