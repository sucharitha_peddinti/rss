package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.Project_ListResponse

class ProjectsListHome_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<Project_ListResponse>
) :
    RecyclerView.Adapter<ProjectsListHome_Adapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProjectsListHome_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.projectslist_screen, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: ProjectsListHome_Adapter.ViewHolder, position: Int) {


        holder.proect_name.text = userList.get(position).name
        Log.i(TAG, "onBindViewHolder: "+userList.get(position).warrenty_days)
        if (userList.get(position).warrenty_days <= 0) {
            holder.freedays_text.visibility=View.GONE
            holder.freedays_btn.visibility=View.VISIBLE
        } else {
            holder.freedays_text.visibility=View.VISIBLE
            holder.freedays_btn.visibility=View.GONE
            holder.freedays_text.text = "" + userList.get(position).warrenty_days + " Days left"
            val anim: Animation = AlphaAnimation(1.0f, 1.0f)
            anim.duration =
                1500 //You can manage the blinking time with this parameter

            anim.startOffset = 20
            anim.repeatMode = Animation.REVERSE
            anim.repeatCount = Animation.INFINITE
            holder.warrantdays_text.startAnimation(anim)
        }

        holder.warrantdays_text.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.warrent_view, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
            //show dialog
            val mAlertDialog = mBuilder.show()
            //login button click of custom layout

            val close_btn = mDialogView.findViewById<ImageView>(R.id.close_btn)
            val ticketid_text = mDialogView.findViewById<TextView>(R.id.ticketid_text)
            val callsupport = mDialogView.findViewById<TextView>(R.id.callsupport)
            ticketid_text.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout

            }

            callsupport.setOnClickListener {
                val dialIntent = Intent(Intent.ACTION_DIAL)
                dialIntent.data = Uri.parse("tel:" + "91008 88838")
                context.startActivity(dialIntent)
            }
            //cancel button click of custom layout


        }

        holder.warrantdays_text.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.warrent_view, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
            //show dialog
            val mAlertDialog = mBuilder.show()
            //login button click of custom layout

            val close_btn = mDialogView.findViewById<ImageView>(R.id.close_btn)
            val ticketid_text = mDialogView.findViewById<TextView>(R.id.ticketid_text)
            val callsupport = mDialogView.findViewById<TextView>(R.id.callsupport)
            ticketid_text.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout

            }

            callsupport.setOnClickListener {
                val dialIntent = Intent(Intent.ACTION_DIAL)
                dialIntent.data = Uri.parse("tel:" + "91008 88838")
                context.startActivity(dialIntent)
            }
            //cancel button click of custom layout


        }
        holder.freedays_btn.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.warrent_view, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
            //show dialog
            val mAlertDialog = mBuilder.show()
            //login button click of custom layout

            val close_btn = mDialogView.findViewById<ImageView>(R.id.close_btn)
            val ticketid_text = mDialogView.findViewById<TextView>(R.id.ticketid_text)
            val callsupport = mDialogView.findViewById<TextView>(R.id.callsupport)
            ticketid_text.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout

            }

            callsupport.setOnClickListener {
                val dialIntent = Intent(Intent.ACTION_DIAL)
                dialIntent.data = Uri.parse("tel:" + "91008 88838")
                context.startActivity(dialIntent)
            }
            //cancel button click of custom layout


        }

        holder.arrow_image.setOnClickListener {

        }



    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val proect_name = itemView.findViewById(R.id.proect_name) as TextView
        val warrantdays_text = itemView.findViewById(R.id.warrantdays_text) as TextView
        val freedays_text = itemView.findViewById(R.id.freedays_text) as TextView
        val arrow_image = itemView.findViewById(R.id.arrow_image) as ImageView
        val freedays_btn = itemView.findViewById(R.id.freedays_btn) as Button


    }
}
