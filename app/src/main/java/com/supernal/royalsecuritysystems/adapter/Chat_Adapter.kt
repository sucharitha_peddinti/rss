package com.supernal.royalsecuritysystems.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.ChatListResponse

class Chat_Adapter(
    var context: Context,
    val userList: java.util.ArrayList<ChatListResponse>
) :
    RecyclerView.Adapter<Chat_Adapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Chat_Adapter.ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.chat_adater, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: Chat_Adapter.ViewHolder, position: Int) {


        holder.text_gchat_message_other.text = userList.get(position).comments
        holder.text_gchat_date_other.text = userList.get(position).created_date.toString()
        holder.text_gchat_user_other.text = userList.get(position).person_name.toString()


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val text_gchat_message_other =
            itemView.findViewById(R.id.text_gchat_message_other) as TextView
        val text_gchat_date_other = itemView.findViewById(R.id.text_gchat_date_other) as TextView
        val text_gchat_user_other = itemView.findViewById(R.id.text_gchat_user_other) as TextView


    }
}
