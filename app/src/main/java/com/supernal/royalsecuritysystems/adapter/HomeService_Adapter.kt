package com.supernal.royalsecuritysystems.adapter

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.models.ServiceListResponse

class HomeService_Adapter(context: Context, arrayListImage: ArrayList<ServiceListResponse>) :
    BaseAdapter() {

    //Passing Values to Local Variables

    var context = context
    var arrayListImage = arrayListImage


    //Auto Generated Method
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var myView = convertView
        var holder: ViewHolder


        if (myView == null) {

            //If our View is Null than we Inflater view using Layout Inflater

            val mInflater = (context as Activity).layoutInflater

            //Inflating our grid_item.
            myView = mInflater.inflate(R.layout.homeservice_adapter, parent, false)

            //Create Object of ViewHolder Class and set our View to it

            holder = ViewHolder()


            //Find view By Id For all our Widget taken in grid_item.

            /*Here !! are use for non-null asserted two prevent From null.
             you can also use Only Safe (?.)

            */


            holder.service_image = myView?.findViewById<ImageView>(R.id.service_image) as ImageView
            holder.servicename_text =
                myView.findViewById<TextView>(R.id.servicename_text) as TextView
            holder.homeservice_linear =
                myView.findViewById<LinearLayout>(R.id.homeservice_linear) as LinearLayout

            holder.homeservice_linear.setOnClickListener {

                val sharedPreferences =
                    context.getSharedPreferences(
                        "loginprefs",
                        Context.MODE_PRIVATE
                    )

                val editor = sharedPreferences.edit()
                editor.putString("service_id", arrayListImage.get(position).id.toString())
                editor.commit()
                val navController =
                    Navigation.findNavController(
                        context as Activity,
                        R.id.nav_host_fragment_content_home
                    )
                navController.navigate(R.id.navigation_servicesdetails)
            }

            //Set A Tag to Identify our view.
            myView.tag = holder


        } else {

            //If Our View in not Null than Just get View using Tag and pass to holder Object.

            holder = myView.tag as ViewHolder

        }

        //Setting Image to ImageView by position

        Glide.with(context).load(arrayListImage.get(position).image)
            .transform(CenterCrop(), RoundedCorners(20))

            .error(R.drawable.logo)
            .into(holder.service_image!!)

        //Setting name to TextView by position
        holder.servicename_text!!.text = arrayListImage.get(position).name

        return myView

    }

    //Auto Generated Method
    override fun getItem(p0: Int): Any {
        return arrayListImage.get(p0)
    }

    //Auto Generated Method
    override fun getItemId(p0: Int): Long {
        return 0
    }

    //Auto Generated Method
    override fun getCount(): Int {
        return arrayListImage.size
    }


    //Create A class To hold over View like we taken in grid_item.xml

    class ViewHolder {

        var service_image: ImageView? = null
        var servicename_text: TextView? = null
        lateinit var homeservice_linear: LinearLayout

    }

}