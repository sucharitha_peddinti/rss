package com.supernal.royalsecuritysystems.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import com.supernal.royalsecuritysystems.R
import org.json.JSONObject

/**
 *  Created by Sucharitha Peddinti on 20/11/21.
 */
class Payment_Activity : Activity(), PaymentResultListener {

    val TAG: String = Payment_Activity::class.toString()

    lateinit var userid: String
    lateinit var invoice_id_: String
    lateinit var request_id: String
    lateinit var service_namee: String
    lateinit var invoice_id: String
    lateinit var pay_amount: TextView
    lateinit var complaint: TextView
    lateinit var orderid: TextView
    lateinit var amount: String
    lateinit var email: String
    lateinit var mobile: String
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_screen)
        /*
        * To ensure faster loading of the Checkout form,
        * call this method as early as possible in your checkout flow
        * */
        Checkout.preload(applicationContext)

        var button: Button = findViewById(R.id.btn_pay)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        userid = sharedPreferences.getString("userid", "").toString()
        invoice_id = sharedPreferences.getString("invoice_id", "").toString()
        invoice_id_ = sharedPreferences.getString("invoice_id_", "").toString()
        request_id = sharedPreferences.getString("request_id", "").toString()
        service_namee = sharedPreferences.getString("service_namee", "").toString()
        amount = sharedPreferences.getString("amount", "").toString()
        mobile = sharedPreferences.getString("mobile", "").toString()
        email = sharedPreferences.getString("email", "").toString()

        pay_amount = findViewById<TextView>(R.id.pay_amount)
        orderid = findViewById<TextView>(R.id.orderid)
        complaint = findViewById<TextView>(R.id.complaint)

        Log.e("serv", service_namee)
        Log.e("inv", invoice_id)
        complaint.text = service_namee
        orderid.text = "" + invoice_id_
        pay_amount.text = "Rs : $amount"

        button.setOnClickListener {
            startPayment()
        }
    }

    private fun startPayment() {
        /*
        *  You need to pass current activity in order to let Razorpay create CheckoutActivity
        * */
        val activity: Activity = this
        val co = Checkout()

        try {
            val final_amount = amount.toInt() * 100
            val options = JSONObject()
            options.put("name", "Royal IT Park Services")
            options.put("description", "Demoing Charges")
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", final_amount)
            options.put("send_sms_hash", true)
            val prefill = JSONObject()
            prefill.put("email", email)
            prefill.put("contact", mobile)
            options.put("prefill", prefill)
            co.open(activity, options)
            // Log.e("amount", "" + final_amount)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }


    override fun onPaymentError(errorCode: Int, response: String?) {
        try {
            Toast.makeText(this, "Payment failed ", Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            Log.e(TAG, "Exception in onPaymentSuccess", e)
        }
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        try {
            Toast.makeText(this, "Payment Successful", Toast.LENGTH_LONG).show()

            razorpayPaymentId?.let {
                //   postPayment(it)

                startActivity(
                    Intent(
                        this@Payment_Activity,
                        Sucess_Activity::class.java
                    )
                )
                val amount=sharedPreferences.getString("amount","")
                finish()
            }

        } catch (e: Exception) {
            Log.e(TAG, "Exception in onPaymentSuccess", e)

        }
    }

//    private fun postPayment(razorpayPaymentId: String) {
//
//        if (NetWorkConection.isNEtworkConnected(this)) {
//            //Set the Adapter to the RecyclerView//
////            progressBarOtp.visibility = View.VISIBLE
//
//            Log.e("amount asa", "" + amount)
//            Log.e("invoice id", "" + invoice_id)
//
//            var apiServices = APIClient.client.create(Api::class.java)
//
//            val call =
//                apiServices.post_paymentData(
//
//                    invoice_id, userid, amount, razorpayPaymentId, "12345",
//                )
//
//
//            call.enqueue(object : Callback<LoginDefaultResponse> {
//                override fun onResponse(
//                    call: Call<LoginDefaultResponse>,
//                    response: Response<LoginDefaultResponse>
//                ) {
//
//                    //progressBarOtp.visibility = View.GONE
//                    Log.e(ContentValues.TAG, "" + response.toString())
//
//                    if (response.isSuccessful) {
//
//                        //Set the Adapter to the RecyclerView//
//
//                        try {
//
////                            Toast.makeText(
////                                this@Payment_Activity,
////                                "Payment Sucess",
////                                Toast.LENGTH_LONG
////                            ).show()
//
////                            startActivity(
////                                Intent(
////                                    this@Payment_Activity,
////                                    Home_Screen::class.java
////                                )
////                            )
//                            finish()
//                        } catch (e: NullPointerException) {
//                            e.printStackTrace()
//                        } catch (e: TypeCastException) {
//                            e.printStackTrace()
//                        }
//
//                    } else {
//
//                        Toast.makeText(
//                            this@Payment_Activity,
//                            "",
//                            Toast.LENGTH_LONG
//                        ).show()
//                    }
//
//                }
//
//
//                override fun onFailure(call: Call<LoginDefaultResponse>, t: Throwable) {
//                    //progressBarOtp.visibility = View.GONE
//                    Log.e(ContentValues.TAG, t.localizedMessage)
//
//
//                }
//            })
//
//
//        } else {
//            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
//
//        }
//
//    }


}