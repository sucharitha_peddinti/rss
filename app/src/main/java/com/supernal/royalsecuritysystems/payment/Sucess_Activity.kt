package com.supernal.royalsecuritysystems.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.supernal.royalsecuritysystems.Home_Activity
import com.supernal.royalsecuritysystems.R

/**
 *  Created by Sucharitha Peddinti on 20/11/21.
 */
class Sucess_Activity : Activity() {


    lateinit var amount: String

    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sucess_screen)
        /*
        * To ensure faster loading of the Checkout form,
        * call this method as early as possible in your checkout flow
        * */

        var done_btn: Button = findViewById(R.id.done_btn)
        var tokenid_text: TextView = findViewById(R.id.tokenid_text)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        amount = sharedPreferences.getString("amount", "").toString()
        val name = sharedPreferences.getString("name", "").toString()

        tokenid_text.text = "Your Payment of Rs." + amount  + " was Successfully Completed."



        done_btn.setOnClickListener {

            startActivity(
                Intent(
                    this@Sucess_Activity,
                    Home_Activity::class.java
                )
            )
            finish()
        }
    }


}