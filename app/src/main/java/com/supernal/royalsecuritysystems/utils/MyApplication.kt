package com.supernal.royalsecuritysystems.utils

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex


class MyApplication : Application() {


    //private Tracker analyticsTracker;

    val CLIENT_ID = "ca_GYYpKLWc1GSy9yEMFDTIVypg90GwKW74"
    val SECRET_KEY = "sk_test_pKY044Ac7Ynkn5D36yrIF1Rb00pQ3q8KYr"
    val CALLBACK_URL =
        "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_GYYpKLWc1GSy9yEMFDTIVypg90GwKW74&scope=read_write"

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(applicationContext)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)


    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}