package com.supernal.royalsecuritysystems

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager

class Splash_Screen : Activity() {
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        sharedPreferences = getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

        // This is used to hide the status bar and make
        // the splash screen as a full screen activity.
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        // we used the postDelayed(Runnable, time) method
        // to send a message with a delayed time.
        Handler().postDelayed({

            val isLogined = sharedPreferences.getBoolean("islogin", false)

            if (isLogined) {

                val intent = Intent(this, Home_Activity::class.java)
                startActivity(intent)
                finish()
            } else {

                val intent = Intent(this, Login_Screen::class.java)
                startActivity(intent)
                finish()
            }


        }, 3000) // 3000 is the delayed time in milliseconds.
    }
}