package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.ActiveTickets_Adapter
import com.supernal.royalsecuritysystems.databinding.ActiveticketsScreenBinding
import com.supernal.royalsecuritysystems.models.ActiveTicketsListResponse
import com.supernal.royalsecuritysystems.models.ActiveTicketsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ActiveTickets_Fragment : Fragment() {
    lateinit var activeticket_adapter: ActiveTickets_Adapter

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: ActiveticketsScreenBinding? = null

    lateinit var recyclerview_activetic: RecyclerView
    lateinit var progressBarat: ProgressBar
    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String
    lateinit var linearhead: LinearLayout
    lateinit var nodata_text: TextView

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = ActiveticketsScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerview_activetic = binding.activeRecler
        progressBarat = binding.progressBarat

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()
        nodata_text = binding.nodataText
        linearhead = binding.linearsu
        binding.serviceText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_support)
        }

        binding.addChat.setOnClickListener {
            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_chat)
        }

        binding.closedticketText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_closedtickets)
        }

        getActiveTicketList()
        return root
    }


    /**
     * This function add the headers to the table
     */

    private fun getActiveTicketList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.active_tickets(getString(R.string.api_key), userid)

            progressBarat.visibility = View.VISIBLE
            call.enqueue(object : Callback<ActiveTicketsResponse> {
                override fun onResponse(
                    call: Call<ActiveTicketsResponse>,
                    response: Response<ActiveTicketsResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarat.visibility = View.GONE

                    if (response.isSuccessful) {

                        val listOfcategories = response.body()?.response

                        //Set the Adapter to the RecyclerView//

                        try {


                            val activeticketslist =
                                response.body()?.response!!



                            if (activeticketslist.isEmpty()) {
                                nodata_text.visibility = View.VISIBLE
                                linearhead.visibility = View.GONE
                                recyclerview_activetic.visibility = View.GONE
                            } else {
                                nodata_text.visibility = View.GONE
                                linearhead.visibility = View.VISIBLE
                                recyclerview_activetic.visibility = View.VISIBLE
                                activeticket_adapter =
                                    ActiveTickets_Adapter(
                                        activity as Activity,
                                        activeticketslist as ArrayList<ActiveTicketsListResponse>
                                    )
                                recyclerview_activetic.adapter =
                                    activeticket_adapter
                                activeticket_adapter.notifyDataSetChanged()
                            }

                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }

                override fun onFailure(call: Call<ActiveTicketsResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarat.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}