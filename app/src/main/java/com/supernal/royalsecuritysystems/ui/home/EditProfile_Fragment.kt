package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.databinding.EditprofileviewFragmentBinding
import com.supernal.royalsecuritysystems.models.ProfileResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditProfile_Fragment : Fragment() {

    private var _binding: EditprofileviewFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var edit_username: EditText
    lateinit var edit_phonenum: EditText
    lateinit var edit_gsit: EditText
    lateinit var edit_useremail: EditText
    lateinit var edit_userwebsite: EditText
    lateinit var profileprogressBar: ProgressBar

    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String
    lateinit var cname_stng: String
    lateinit var fname_stng: String
    lateinit var gname_stng: String
    lateinit var website_stng: String
    lateinit var mobile_stng: String
    lateinit var email_stng: String
    lateinit var address_stng: String
    lateinit var gst_stng: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = EditprofileviewFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        edit_username = binding.editUsername
        edit_phonenum = binding.editPhonenum
        profileprogressBar = binding.profileprogressBar
        edit_useremail = binding.editUseremail
        edit_useremail = binding.editUseremail

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()


        val fname = sharedPreferences.getString("fname", "")
        val email = sharedPreferences.getString("email", "")
        val mobilenum = sharedPreferences.getString("mobilenum", "")
        val cname = sharedPreferences.getString("cname", "")
        val gst = sharedPreferences.getString("gst", "")
        val website = sharedPreferences.getString("website", "")
        val address = sharedPreferences.getString("address", "")

        binding.editUsername.setText("" + fname)
        binding.editCompanyname.setText("" + cname)
        binding.editPhonenum.setText("" + mobilenum)
        binding.editUseremail.setText("" + email)
        binding.editUsergst.setText("" + gst)
        binding.userwebsite.setText("" + website)
        binding.useraddress.setText("" + address)

        binding.submitBtn.setOnClickListener {

            getProjectDetails()

        }

        return root
    }

    private fun getProjectDetails() {

        if (NetWorkConection.isNEtworkConnected(requireActivity())) {

            //Set the Adapter to the RecyclerView//

            fname_stng = binding.editUsername.text.toString().trim()
            cname_stng = binding.editCompanyname.text.toString().trim()
            email_stng = binding.editUseremail.text.toString().trim()
            mobile_stng = binding.editPhonenum.text.toString().trim()
            gst_stng = binding.editUsergst.text.toString().trim()
            website_stng = binding.userwebsite.text.toString().trim()
            address_stng = binding.useraddress.text.toString().trim()

            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.updateProfileDetails(
                    getString(R.string.api_key), fname_stng, cname_stng,
                    gst_stng, address_stng, website_stng, userid
                )

            profileprogressBar.visibility = View.VISIBLE
            call.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    profileprogressBar.visibility = View.GONE

                    try {


                        if (response.isSuccessful) {

                            //Set the Adapter to the RecyclerView//

                            val profilelist =
                                response.body()?.response!!
                            val navController =
                                Navigation.findNavController(
                                    requireActivity() as Activity,
                                    R.id.nav_host_fragment_content_home
                                )
                            navController.navigate(R.id.nav_myprofile)

                        }
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }
//

                }


                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    profileprogressBar.visibility = View.GONE


                }

            })


        } else {

            Toast.makeText(
                requireActivity(),
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}