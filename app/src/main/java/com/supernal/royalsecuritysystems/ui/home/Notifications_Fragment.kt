package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.rss.royalit.model.NotificationssListResponse
import com.supernal.royalsecuritysystems.models.NotificationssResponse
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.Notifications_Adapter
import com.supernal.royalsecuritysystems.databinding.NotificationScreenBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 *  Created by Sucharitha Peddinti on 14/11/21.
 */
class Notifications_Fragment : Fragment() {

    lateinit var swiprerefresh_: SwipeRefreshLayout

    private var _binding: NotificationScreenBinding? = null
    lateinit var notification_adapter: Notifications_Adapter

    lateinit var projectstatus_text: TextView
    lateinit var nodata_text: TextView
    lateinit var notificationRecycler: RecyclerView
    lateinit var progressBarknow: ProgressBar
    lateinit var sharedPreferences: SharedPreferences

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var userid: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = NotificationScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        progressBarknow = binding.progressBarnoti
        notificationRecycler = binding.notificationRecycler
        nodata_text = binding.nodataText

        swiprerefresh_ = binding.swipeRefresh

        swiprerefresh_.setOnRefreshListener {

            // reset the SwipeRefreshLayout (stop the loading spinner)
            swiprerefresh_.isRefreshing = false
            getNotificationLsit()
        }


        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()


        getNotificationLsit()
        return root
    }


    private fun getNotificationLsit() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.notification_msgs_data(getString(R.string.api_key), userid)

            progressBarknow.visibility = View.VISIBLE
            call.enqueue(object : Callback<NotificationssListResponse> {
                override fun onResponse(
                    call: Call<NotificationssListResponse>,
                    response: Response<NotificationssListResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarknow.visibility = View.GONE

                    if (response.isSuccessful) {


                        //Set the Adapter to the RecyclerView//

                        try {

                            val activeticketslist =
                                response.body()?.response!!

                            if (activeticketslist.isEmpty()) {
                                nodata_text.visibility = View.VISIBLE
                                notificationRecycler.visibility = View.GONE
                            } else {
                                nodata_text.visibility = View.GONE
                                notificationRecycler.visibility = View.VISIBLE

                                notification_adapter =
                                    Notifications_Adapter(
                                        requireActivity(),
                                        activeticketslist as ArrayList<NotificationssResponse>
                                    )
                                notificationRecycler.adapter =
                                    notification_adapter
                                notification_adapter.notifyDataSetChanged()

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }
                    }


                }

                override fun onFailure(call: Call<NotificationssListResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarknow.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}