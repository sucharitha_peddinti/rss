package com.supernal.royalsecuritysystems.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.Services_Adapter
import com.supernal.royalsecuritysystems.databinding.ServiceScreenBinding
import com.supernal.royalsecuritysystems.models.ServiceListResponse
import com.supernal.royalsecuritysystems.models.ServiceResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class Service_Fragment : Fragment() {

    private var _binding: ServiceScreenBinding? = null
    lateinit var service_adapter: Services_Adapter
    lateinit var progressBarservice: ProgressBar
    lateinit var recyclerview_serive: RecyclerView

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = ServiceScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerview_serive = binding.recyclerviewService
        progressBarservice = binding.progressBarservice

        recyclerview_serive.layoutManager =
            LinearLayoutManager(activity as Activity, LinearLayout.VERTICAL, false)


        getServiceList()
        return root
    }

    private fun getServiceList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.getServicesList(getString(R.string.api_key))

            progressBarservice.visibility = View.VISIBLE
            call.enqueue(object : Callback<ServiceResponse> {
                override fun onResponse(
                    call: Call<ServiceResponse>,
                    response: Response<ServiceResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarservice.visibility = View.GONE

                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        try {


                            val selectedserviceslist =
                                response.body()?.response!!

                            service_adapter =
                                Services_Adapter(
                                    requireActivity(),
                                    selectedserviceslist as ArrayList<ServiceListResponse>
                                )
                            recyclerview_serive.adapter =
                                service_adapter
                            service_adapter.notifyDataSetChanged()

                        } catch (e: IllegalStateException) {
                            e.printStackTrace()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }

                    }


                }

                override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarservice.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}