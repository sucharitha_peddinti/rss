package com.supernal.royalsecuritysystems.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.supernal.royalsecuritysystems.databinding.TermsconditionsScreenBinding


class Terms_ConditionsFragment : Fragment() {

    private var _binding: TermsconditionsScreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = TermsconditionsScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.termsTerms.settings.javaScriptEnabled = true

        binding.termsTerms.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                binding.progress.visibility = View.GONE

            }

            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {

            }
        }
        binding.termsTerms.webViewClient =  object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url!!)
                binding.progress.visibility = View.GONE

                return true
            }

        }
        binding.termsTerms.loadUrl("https://royalitpark.com/Terms_Conditions")

        return root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}