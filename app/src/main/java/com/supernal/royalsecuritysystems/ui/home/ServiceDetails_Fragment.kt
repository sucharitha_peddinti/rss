package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.databinding.ServicedetailsScreenBinding
import com.supernal.royalsecuritysystems.models.ServiceDetailsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServiceDetails_Fragment : Fragment() {

    private var _binding: ServicedetailsScreenBinding? = null

    lateinit var servicedtails_image: ImageView
    lateinit var servicetitle_text: TextView
    lateinit var servicedet_text: TextView
    lateinit var progressBarservv: ProgressBar
    lateinit var sharedPreferences: SharedPreferences
    lateinit var service_id: String
    lateinit var removetags: String
    lateinit var enquir_btn: TextView

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = ServicedetailsScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        servicedtails_image = binding.servicedtailsImage
        servicetitle_text = binding.servicetitleText
        servicedet_text = binding.servicedetText
        enquir_btn = binding.enquirBtn
        progressBarservv = binding.progressBarservv


        enquir_btn.setOnClickListener {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:" + "9100888838")
            startActivity(dialIntent)
        }

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        service_id = sharedPreferences.getString("service_id", "").toString()
        getServiceDetailsList()

        return root
    }


    private fun getServiceDetailsList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.servicesdetailsList(getString(R.string.api_key), service_id)

            progressBarservv.visibility = View.VISIBLE
            call.enqueue(object : Callback<ServiceDetailsResponse> {
                override fun onResponse(
                    call: Call<ServiceDetailsResponse>,
                    response: Response<ServiceDetailsResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarservv.visibility = View.GONE

                    try {


                    if (response.isSuccessful) {


                        //Set the Adapter to the RecyclerView//

                        val servicelist =
                            response.body()?.response!!

                        servicetitle_text.text = servicelist.name

                        removetags = Html.fromHtml(servicelist.content).toString()

                        Log.e("removetags", removetags)
                        servicedet_text.text = removetags

                        Glide.with(this@ServiceDetails_Fragment).load(servicelist.image)
                            .apply(RequestOptions().centerCrop())
                            .transform(CenterCrop(), RoundedCorners(20))
                            .error(R.drawable.logo)
                            .into(servicedtails_image)
                    }

                    } catch (e : NullPointerException){
                        e.printStackTrace()
                    } catch (e : Exception){
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<ServiceDetailsResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarservv.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}