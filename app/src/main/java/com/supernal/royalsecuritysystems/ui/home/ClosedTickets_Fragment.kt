package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.ClosedTickets_Adapter
import com.supernal.royalsecuritysystems.databinding.ClosedticketsScreenBinding
import com.supernal.royalsecuritysystems.models.ActiveTicketsListResponse
import com.supernal.royalsecuritysystems.models.ActiveTicketsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ClosedTickets_Fragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: ClosedticketsScreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var closedticket_adapter: ClosedTickets_Adapter
    lateinit var progressBarct: ProgressBar
    lateinit var recyclerview_closedtick: RecyclerView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var linearhead: LinearLayout
    lateinit var nodata_text: TextView
    lateinit var userid: String
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = ClosedticketsScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()
        recyclerview_closedtick = binding.closedRecler
        progressBarct = binding.progressBarct

        nodata_text = binding.nodataText
        linearhead = binding.linearsu
        binding.serviceText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_support)
        }

        binding.activeticketsText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_activetickets)
        }

        getClosedTicketList()
        return root
    }

    private fun getClosedTicketList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.closed_tickets(getString(R.string.api_key), userid)

            progressBarct.visibility = View.VISIBLE
            call.enqueue(object : Callback<ActiveTicketsResponse> {
                override fun onResponse(
                    call: Call<ActiveTicketsResponse>,
                    response: Response<ActiveTicketsResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarct.visibility = View.GONE

                    if (response.isSuccessful) {


                        //Set the Adapter to the RecyclerView//

                        try {

                            val activeticketslist =
                                response.body()?.response!!

                            if (activeticketslist.isEmpty()) {
                                nodata_text.visibility = View.VISIBLE
                                linearhead.visibility = View.GONE
                                recyclerview_closedtick.visibility = View.GONE
                            } else {
                                nodata_text.visibility = View.GONE
                                linearhead.visibility = View.VISIBLE
                                recyclerview_closedtick.visibility = View.VISIBLE

                                closedticket_adapter =
                                    ClosedTickets_Adapter(
                                        requireActivity(),
                                        activeticketslist as ArrayList<ActiveTicketsListResponse>
                                    )
                                recyclerview_closedtick.adapter =
                                    closedticket_adapter
                                closedticket_adapter.notifyDataSetChanged()

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }
                    }


                }

                override fun onFailure(call: Call<ActiveTicketsResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarct.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}