package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.databinding.ProfileviewFragmentBinding
import com.supernal.royalsecuritysystems.models.ProfileResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Profile_Fragment : Fragment() {

    private var _binding: ProfileviewFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var username: TextView
    lateinit var phonenum: TextView
    lateinit var gsit: TextView
    lateinit var useremail: TextView
    lateinit var profileprogressBar: ProgressBar

    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = ProfileviewFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        username = binding.username
        phonenum = binding.phonenum
        gsit = binding.gsit
        profileprogressBar = binding.profileprogressBar
        useremail = binding.useremail

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()

        binding.editImageview.setOnClickListener {
            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_ediprofile)
        }

        getProjectDetails()
        return root
    }

    private fun getProjectDetails() {

        if (NetWorkConection.isNEtworkConnected(requireActivity())) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.getProfileDetails(getString(R.string.api_key), userid)

            profileprogressBar.visibility = View.VISIBLE
            call.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    profileprogressBar.visibility = View.GONE

                    try {


                        if (response.isSuccessful) {

                            //Set the Adapter to the RecyclerView//

                            val profilelist =
                                response.body()?.response!!


                            username.text = profilelist.name
                            useremail.text = profilelist.email
                            phonenum.text = profilelist.mobile
                            gsit.text = profilelist.gstin

                        }
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
//

                }


                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    profileprogressBar.visibility = View.GONE


                }

            })


        } else {

            Toast.makeText(
                requireActivity(),
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}