package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.PaidInvoice_Adapter
import com.supernal.royalsecuritysystems.databinding.InvoiceScreenBinding
import com.supernal.royalsecuritysystems.models.InvoiceListResponse
import com.supernal.royalsecuritysystems.models.InvoiceResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class Invoice_Fragment : Fragment() {

    private var _binding: InvoiceScreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var recyclerview_paid: RecyclerView
    lateinit var nodata_text: TextView
    lateinit var progressBar_invoice: ProgressBar
    lateinit var paidinvoice_adapter: PaidInvoice_Adapter
    lateinit var linearhead: LinearLayout

    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = InvoiceScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerview_paid = binding.invoiceRecler
        nodata_text = binding.nodataText
        progressBar_invoice = binding.progressBarinvoice
        linearhead = binding.linearhead

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()

        binding.nonpaidText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_nonpaid)
        }
        binding.ppPaidText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_partialpaid)
        }





        getInvoiceList()
        return root
    }

    private fun getInvoiceList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.alltypes_invoices(getString(R.string.api_key), userid, "1")

            progressBar_invoice.visibility = View.VISIBLE
            call.enqueue(object : Callback<InvoiceResponse> {
                override fun onResponse(
                    call: Call<InvoiceResponse>,
                    response: Response<InvoiceResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBar_invoice.visibility = View.GONE

                    if (response.isSuccessful) {


                        //Set the Adapter to the RecyclerView//

                        try {


                            val activeticketslist =
                                response.body()?.response!!



                            if (activeticketslist.isEmpty()) {
                                nodata_text.visibility = View.VISIBLE
                                linearhead.visibility = View.GONE
                                recyclerview_paid.visibility = View.GONE
                            } else {

                                nodata_text.visibility = View.GONE
                                linearhead.visibility = View.VISIBLE
                                recyclerview_paid.visibility = View.VISIBLE
                                paidinvoice_adapter =
                                    PaidInvoice_Adapter(
                                        activity as Activity,
                                        activeticketslist as ArrayList<InvoiceListResponse>
                                    )
                                recyclerview_paid.adapter =
                                    paidinvoice_adapter
                                paidinvoice_adapter.notifyDataSetChanged()
                            }




                    } catch (e : NullPointerException){
                        e.printStackTrace()
                    } catch (e : Exception){
                        e.printStackTrace()
                    }
                    }
                }

                override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBar_invoice.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}