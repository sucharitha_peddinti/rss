package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.databinding.SupportScreenBinding
import com.supernal.royalsecuritysystems.models.LoginDefaultResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Support_Fragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: SupportScreenBinding? = null

    lateinit var progressBarserv: ProgressBar
    lateinit var fullname_edit: EditText
    lateinit var companyname_edit: EditText
    lateinit var email_edit: EditText
    lateinit var mobile_edit: EditText
    lateinit var service_prodcut_edit: EditText
    lateinit var complaintdetails_edit: EditText
    lateinit var serv_submit_btn: Button

    lateinit var fullname_stng: String
    lateinit var companyname_stng: String
    lateinit var email_stng: String
    lateinit var mobile_stng: String
    lateinit var service_prodcut_stng: String
    lateinit var complaintdetails_stng: String

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String

    var pattern =
        "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"

    val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = SupportScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        userid = sharedPreferences.getString("userid", "").toString()

        fullname_edit = binding.serviFullnameEdit
        companyname_edit = binding.servCompnameEdit
        email_edit = binding.serviceEmailEdit
        mobile_edit = binding.serviceMobileEdit
        service_prodcut_edit = binding.serviceProductEdit
        complaintdetails_edit = binding.serviceReairEdit
        serv_submit_btn = binding.servSubmitBtn
        progressBarserv = binding.progressBarserv

        fullname_edit.setText(sharedPreferences.getString("name", "").toString())
        email_edit.setText(sharedPreferences.getString("email", "").toString())
        mobile_edit.setText(sharedPreferences.getString("mobile", "").toString())
        companyname_edit.setText(sharedPreferences.getString("companyname", "").toString())
        binding.servSubmitBtn.setOnClickListener {

            companyname_stng = companyname_edit.text.toString()
            fullname_stng = fullname_edit.text.toString()
            email_stng = email_edit.text.toString()
            mobile_stng = mobile_edit.text.toString()
            complaintdetails_stng = complaintdetails_edit.text.toString()
            service_prodcut_stng = service_prodcut_edit.text.toString()
//            if (fullname_stng.isEmpty()) {
//                fullname_edit.error = "Please Enter UserName"
//            } else if (companyname_stng.isEmpty()) {
//                companyname_edit.error = "Please Enter Compan Name"
//            } else if (mobile_stng.isEmpty()) {
//                mobile_edit.error = "Please Enter Phone Number"
//            } else if (mobile_stng.length < 10) {
//                mobile_edit.error = "Please Enter 10 digits Phone Number"
//
//            } else if (!mobile_stng.matches(pattern.toRegex())) {
//                mobile_edit.error = "Please Enter valid Phone Number"
//
//            } else if (email_stng.isEmpty()) {
//                email_edit.error = "Please Enter Email Address"
//            } else if (!email_stng.matches(emailPattern.toRegex())) {
//                email_edit.error = "Please Enter valid Email Address"
//            }
            if (service_prodcut_stng.isEmpty()) {
                service_prodcut_edit.error = "Please Enter Service Details"
            } else if (complaintdetails_stng.isEmpty()) {
                complaintdetails_edit.error = "Please Enter Complaint Details"
            } else {
                postSupportRequest()
            }
        }
        binding.activeticketsText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_activetickets)
        }

        binding.closedticketText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_closedtickets)
        }

        return root
    }

    private fun postSupportRequest() {

        if (NetWorkConection.isNEtworkConnected(requireActivity())) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.service_request(
                    getString(R.string.api_key),
                    userid,
                    fullname_stng,
                    companyname_stng,
                    email_stng,
                    mobile_stng,
                    service_prodcut_stng, complaintdetails_stng
                )
            Log.e("companyname_stng", companyname_stng)

            progressBarserv.visibility = View.VISIBLE

            call.enqueue(object : Callback<LoginDefaultResponse> {
                override fun onResponse(
                    call: Call<LoginDefaultResponse>,
                    response: Response<LoginDefaultResponse>
                ) {

                    progressBarserv.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.body()!!.message)


                    val codee = response.body()?.code
                    val status = response.body()?.status
                    if ((status!!.equals("true") || (status == true))) {
                        if ((codee!!.equals("1") || (codee == 1))) {

                            //Set the Adapter to the RecyclerView//

                            try {


                                val listOfcategories = response.body()?.response

                                Toast.makeText(
                                    requireActivity(),
                                    "" + response.body()?.message,
                                    Toast.LENGTH_LONG
                                ).show()


                                val navController =
                                    Navigation.findNavController(
                                        requireActivity() as Activity,
                                        R.id.nav_host_fragment_content_home
                                    )
                                navController.navigate(R.id.navigation_activetickets)
//                                val intent = Intent(
//                                    requireActivity(),
//                                    Home_Activity::class.java
//                                )
//                                startActivity(intent)

                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                            } catch (e: TypeCastException) {
                                e.printStackTrace()
                            }

                        }

                    } else {
                        Toast.makeText(
                            requireActivity(),
                            "" + response.body()?.message,
                            Toast.LENGTH_LONG
                        ).show()

                    }
                }


                override fun onFailure(call: Call<LoginDefaultResponse>, t: Throwable) {
                    progressBarserv.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.toString())
                }
            })


        } else {
            Toast.makeText(requireActivity(), "Please Check your internet", Toast.LENGTH_LONG)
                .show()

        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}