package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.Brands_Adapter
import com.supernal.royalsecuritysystems.adapter.HomeService_Adapter
import com.supernal.royalsecuritysystems.adapter.ProjectsListHome_Adapter
import com.supernal.royalsecuritysystems.adapter.ShopSlideImage_Adapter
import com.supernal.royalsecuritysystems.databinding.FragmentHomeBinding
import com.supernal.royalsecuritysystems.models.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    private var imagesliderpager: ViewPager? = null
    private var currentPage = 0
    private var NUM_PAGES = 0
    lateinit var services_grid: GridView
    lateinit var proect_name: TextView
    lateinit var projectslist_linear: LinearLayout
    lateinit var services_adapter: HomeService_Adapter
    lateinit var projectlist_adapter: ProjectsListHome_Adapter
    lateinit var brands_Adapter: Brands_Adapter
    lateinit var progressBarhome: ProgressBar
    lateinit var brandslist_recclerview: RecyclerView
    lateinit var projectslist: RecyclerView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String
    private var imageModelArrayList: ArrayList<ImageModel>? = null

    //    lateinit var shopcircleindicator: CirclePageIndicator
    private val myImageList = intArrayOf(
        R.drawable.homeimage,
        R.drawable.homeimage,
        R.drawable.homeimage
    )

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    var title: String = ""
    var desctitle: String = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        services_grid = binding.servicesGrid

        projectslist_linear = binding.projectslistLinear
        brandslist_recclerview = binding.brandslistRecclerview
        projectslist = binding.projectslist
        progressBarhome = binding.progressBarhome

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()


        brandslist_recclerview.layoutManager = LinearLayoutManager(
            requireActivity(),
            LinearLayoutManager.HORIZONTAL,
            false
        )

        projectslist.layoutManager = LinearLayoutManager(
            requireActivity(),
            LinearLayoutManager.HORIZONTAL,
            false
        )


        imagesliderpager = root.findViewById<ViewPager>(R.id.imagesliderpager)

        imagesliderpager!!.clipToPadding = false
        imagesliderpager!!.pageMargin = 1
        imagesliderpager!!.setPadding(5, 0, 5, 0)


        getServiceList()
        getBrandsList()
        getWhatsnewList()
        getProjectsList()

        if (!sharedPreferences.getBoolean("importantnote", false)) {
            showDialog()
        }

        return root
    }


    //images auto slide
    private fun populateList(): ArrayList<ImageModel> {

        val list = ArrayList<ImageModel>()

        for (i in 0..2) {
            val imageModel = ImageModel()
            imageModel.setImage_drawables(myImageList[i])
            list.add(imageModel)
            binding.dot3.setViewPager(binding.imagesliderpager)
        }

        return list
    }

    private fun getServiceList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.getServicesList(getString(R.string.api_key))

            progressBarhome.visibility = View.VISIBLE
            call.enqueue(object : Callback<ServiceResponse> {
                override fun onResponse(
                    call: Call<ServiceResponse>,
                    response: Response<ServiceResponse>,
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarhome.visibility = View.GONE

                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        val selectedserviceslist =
                            response.body()?.response!!

                        services_adapter =
                            HomeService_Adapter(
                                activity as Activity,
                                selectedserviceslist as ArrayList<ServiceListResponse>
                            )
                        services_grid.adapter =
                            services_adapter
                        services_adapter.notifyDataSetChanged()

                    }


                }

                override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarhome.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    private fun getProjectsList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.client_projects(getString(R.string.api_key), userid)

            progressBarhome.visibility = View.VISIBLE
            call.enqueue(object : Callback<ProjectResponse> {
                override fun onResponse(
                    call: Call<ProjectResponse>,
                    response: Response<ProjectResponse>,
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarhome.visibility = View.GONE

                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        val selectedserviceslist =
                            response.body()?.response!!

                        if (selectedserviceslist.isEmpty()) {

                            projectslist_linear.visibility = View.GONE
                        } else {
                            projectslist_linear.visibility = View.VISIBLE

                            projectlist_adapter =
                                ProjectsListHome_Adapter(
                                    activity as Activity,
                                    selectedserviceslist as ArrayList<Project_ListResponse>
                                )
                            projectslist.adapter =
                                projectlist_adapter
                            projectlist_adapter.notifyDataSetChanged()

                            projectslist.post(Runnable {
                                projectslist.scrollToPosition(projectlist_adapter.itemCount)
                                // Here adapter.getItemCount()== child count
                            })

                        }
                    }


                }

                override fun onFailure(call: Call<ProjectResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarhome.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    private fun getWhatsnewList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.get_whatsnewdata(getString(R.string.api_key))

            progressBarhome.visibility = View.VISIBLE
            call.enqueue(object : Callback<WhatsnewResponse> {
                override fun onResponse(
                    call: Call<WhatsnewResponse>,
                    response: Response<WhatsnewResponse>,
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarhome.visibility = View.GONE

                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        val selectimagelist = response.body()!!.response
                        val selectimagelist_image = selectimagelist.map {


                            val im = it.image
                            val list = ArrayList<String>()
//
//                            list.add(im)
//                            for (i in 0..list.size) {
//                                val imageModel = ImageModel()
//                                imageModel.setImage_drawables(i)
//                                list.add(imageModel.toString())
//                            }
                            imagesliderpager!!.adapter =

                                ShopSlideImage_Adapter(
                                    activity as Activity,
                                    selectimagelist as ArrayList<WhatsnewListResponse>

                                )

                            // shopcircleindicator!!.setViewPager(imagesliderpager)

                            val density = resources.displayMetrics.density

                            //Set circle indicator radius
                            // shopcircleindicator.radius = 4 * density

                            NUM_PAGES = selectimagelist.size

                            // Auto start of viewpager


                        }
                        val handler = Handler()
                        val Update = Runnable {
                            if (currentPage == NUM_PAGES) {
                                currentPage = 0
                            }
                            imagesliderpager!!.setCurrentItem(currentPage++, true)

                        }
                        binding.dot3.setViewPager(binding.imagesliderpager)

                        val swipeTimer = Timer()
                        swipeTimer.schedule(object : TimerTask() {
                            override fun run() {
                                handler.post(Update)
                            }
                        }, 10000, 8000)

//

                    }


                }

                override fun onFailure(call: Call<WhatsnewResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarhome.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    private fun getBrandsList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.getBrandsList(getString(R.string.api_key))

            progressBarhome.visibility = View.VISIBLE
            call.enqueue(object : Callback<BrandsResponse> {
                override fun onResponse(
                    call: Call<BrandsResponse>,
                    response: Response<BrandsResponse>,
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarhome.visibility = View.GONE

                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        val selectedserviceslist =
                            response.body()?.response!!

                        brands_Adapter =
                            Brands_Adapter(
                                activity as Activity,
                                selectedserviceslist as ArrayList<BrandsListResponse>
                            )
                        brandslist_recclerview.adapter =
                            brands_Adapter
                        brands_Adapter.notifyDataSetChanged()

                    }
                }

                override fun onFailure(call: Call<BrandsResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarhome.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    private fun showDialog() {
        val dialog = Dialog(requireContext(), R.style.CustomDialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.adslayout)
        val title_TV = dialog.findViewById(R.id.title_TV) as TextView
        val closeIV = dialog.findViewById(R.id.closeIV) as ImageView
        val desctitle_TV = dialog.findViewById(R.id.desctitle_TV) as TextView
        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.client_important_notes(getString(R.string.api_key))

            progressBarhome.visibility = View.VISIBLE
            call.enqueue(object : Callback<ClientResponse> {
                override fun onResponse(
                    call: Call<ClientResponse>,
                    response: Response<ClientResponse>,
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBarhome.visibility = View.GONE

                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        val selectedserviceslist =
                            response.body()?.Response

                        title_TV.text = selectedserviceslist!!.title
                        desctitle_TV.text = selectedserviceslist.description

                    }
                }

                override fun onFailure(call: Call<ClientResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBarhome.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }


        closeIV.setOnClickListener {
            val editor = sharedPreferences.edit()
            editor.putBoolean("importantnote", true)
            editor.commit()
            dialog.dismiss()
        }

        dialog.show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}