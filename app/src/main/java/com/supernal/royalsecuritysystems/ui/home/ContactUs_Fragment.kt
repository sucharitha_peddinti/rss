package com.supernal.royalsecuritysystems.ui.home

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.google.gson.JsonSyntaxException
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.databinding.ContactusScreenBinding
import com.supernal.royalsecuritysystems.models.LoginDefaultResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUs_Fragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: ContactusScreenBinding? = null

    lateinit var fullname_edit: EditText
    lateinit var email_edit: EditText
    lateinit var mobile_edit: EditText
    lateinit var desc_edit: EditText
    lateinit var desc_stng: String
    lateinit var mobile_stng: String
    lateinit var email_stng: String
    lateinit var fullname_stng: String
    lateinit var contactus_btn: Button
    lateinit var progressBarcontact: ProgressBar
    lateinit var sharedPreferences: SharedPreferences

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var userid: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = ContactusScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        fullname_edit = binding.fullnameEdit
        email_edit = binding.emailEdit
        mobile_edit = binding.mobileEdit
        desc_edit = binding.descEdit
        contactus_btn = binding.contactusBtn

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()
        progressBarcontact = binding.progressBarcontact
        binding.callaction.setOnClickListener {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:" + "91008 88838")
           startActivity(dialIntent)
        }
        contactus_btn.setOnClickListener {

            fullname_stng = fullname_edit.text.toString()
            email_stng = email_edit.text.toString()
            mobile_stng = mobile_edit.text.toString()
            desc_stng = desc_edit.text.toString()

            if (fullname_stng.isEmpty()) {
                fullname_edit.error = "Please Enter Full Name"
            } else if (email_stng.isEmpty()) {
                email_edit.error = "Please Enter Email Address"
            } else if (mobile_stng.isEmpty()) {
                mobile_edit.error = "Please Enter Mobile Number"

            } else if (desc_stng.isEmpty()) {
                desc_edit.error = "Please Enter Description"
            } else {
                postContactUs()

            }
        }
        return root
    }

    private fun postContactUs() {

        if (NetWorkConection.isNEtworkConnected(requireActivity())) {

            //Set the Adapter to the RecyclerView//

            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.contactus_api(
                    getString(R.string.api_key),
                    userid,
                    fullname_stng, email_stng, mobile_stng, desc_stng
                )
            Log.e("mobile_stng", mobile_stng)
            progressBarcontact.visibility = View.VISIBLE

            call.enqueue(object : Callback<LoginDefaultResponse> {
                override fun onResponse(
                    call: Call<LoginDefaultResponse>,
                    response: Response<LoginDefaultResponse>
                ) {
                    progressBarcontact.visibility = View.GONE

                    Log.e(ContentValues.TAG, response.body()!!.message)

                    val status_code = response.body()?.code
                    val status = response.body()?.status
                    if ((status!!.equals(true) || (status == true))) {
                        if ((status_code!!.equals(1) || (status_code == 1))) {

                            //Set the Adapter to the RecyclerView//

                            try {


                                Toast.makeText(
                                    requireActivity(),
                                    "" + response.body()?.response,
                                    Toast.LENGTH_LONG
                                ).show()

                                fullname_edit.setText("")
                                email_edit.setText("")
                                mobile_edit.setText("")
                                desc_edit.setText("")

                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                            } catch (e: TypeCastException) {
                                e.printStackTrace()
                            } catch (e: JsonSyntaxException) {
                                e.printStackTrace()
                            } catch (e: IllegalStateException) {
                                e.printStackTrace()
                            }

                        }
                    }

                }

                override fun onFailure(call: Call<LoginDefaultResponse>, t: Throwable) {
                    progressBarcontact.visibility = View.GONE
                    Log.e(ContentValues.TAG, call.toString())

                }
            })


        } else {
            Toast.makeText(requireActivity(), "Please Check your internet", Toast.LENGTH_LONG)
                .show()

        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}