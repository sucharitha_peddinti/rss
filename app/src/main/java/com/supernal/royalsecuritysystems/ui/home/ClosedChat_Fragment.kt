package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.MessageChat_Adapter
import com.supernal.royalsecuritysystems.databinding.ClosedChatScreenBinding
import com.supernal.royalsecuritysystems.models.ChatListResponse
import com.supernal.royalsecuritysystems.models.ChatResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ClosedChat_Fragment : Fragment() {
    lateinit var chat_adapter: MessageChat_Adapter

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: ClosedChatScreenBinding? = null

    lateinit var recyclerview_activetic: RecyclerView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String
    lateinit var ticket_id: String
    lateinit var edit_gchat_message: EditText
    lateinit var chat_message: String
    lateinit var button_gchat_send: ImageView
    lateinit var linearhead: LinearLayout
    lateinit var nodata_text: TextView

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = ClosedChatScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerview_activetic = binding.recyclerGchat
        edit_gchat_message = binding.editGchatMessage
        binding.buttonGchatSend.setOnClickListener {

            chat_message = edit_gchat_message.text.toString()

            getChatsend()
        }


        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()
        ticket_id = sharedPreferences.getString("ticket_id", "").toString()



        getChatList()
        return root
    }


    /**
     * This function add the headers to the table
     */

    private fun getChatList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.client_invoice_comments(getString(R.string.api_key), ticket_id)

            //progressBarat.visibility = View.VISIBLE
            call.enqueue(object : Callback<ChatResponse> {
                override fun onResponse(
                    call: Call<ChatResponse>,
                    response: Response<ChatResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    // progressBarat.visibility = View.GONE

                    if (response.isSuccessful) {

                        val listOfcategories = response.body()?.response

                        //Set the Adapter to the RecyclerView//


                        val activeticketslist =
                            response.body()?.response!!


                        try {


                            chat_adapter =
                                MessageChat_Adapter(
                                    activity as Activity,
                                    activeticketslist as ArrayList<ChatListResponse>
                                )
                            recyclerview_activetic.adapter =
                                chat_adapter
                            chat_adapter.notifyDataSetChanged()

                        } catch (e: Exception) {
                            e.printStackTrace()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }
                    }


                }

                override fun onFailure(call: Call<ChatResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    //progressBarat.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    private fun getChatsend() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.invoice_comments_save(
                    getString(R.string.api_key),
                    userid,
                    ticket_id,
                    chat_message
                )


            //progressBarat.visibility = View.VISIBLE
            call.enqueue(object : Callback<ChatResponse> {
                override fun onResponse(
                    call: Call<ChatResponse>,
                    response: Response<ChatResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    // progressBarat.visibility = View.GONE

                    if (response.isSuccessful) {

                        val listOfcategories = response.body()?.response

                        //Set the Adapter to the RecyclerView//


                        val activeticketslist =
                            response.body()?.response!!


                        edit_gchat_message.setText("")


                        chat_adapter =
                            MessageChat_Adapter(
                                activity as Activity,
                                activeticketslist as ArrayList<ChatListResponse>
                            )
                        recyclerview_activetic.adapter =
                            chat_adapter
                        chat_adapter.notifyDataSetChanged()

                    }


                }

                override fun onFailure(call: Call<ChatResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    //progressBarat.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}