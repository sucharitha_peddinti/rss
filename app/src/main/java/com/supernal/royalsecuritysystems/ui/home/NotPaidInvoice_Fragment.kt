package com.supernal.royalsecuritysystems.ui.home

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.erepairs.app.api.APIClient
import com.erepairs.app.api.Api
import com.erepairs.app.utils.NetWorkConection
import com.supernal.royalsecuritysystems.R
import com.supernal.royalsecuritysystems.adapter.NotPaidInvoice_Adapter
import com.supernal.royalsecuritysystems.databinding.NotpaidScreenBinding
import com.supernal.royalsecuritysystems.models.InvoiceListResponse
import com.supernal.royalsecuritysystems.models.InvoiceResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class NotPaidInvoice_Fragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: NotpaidScreenBinding? = null
    lateinit var recyclerview_paid: RecyclerView
    lateinit var progressBar_invoice: ProgressBar
    lateinit var notpaidinvoice_adapter: NotPaidInvoice_Adapter
    lateinit var sharedPreferences: SharedPreferences
    lateinit var userid: String
    lateinit var nodata_text: TextView
    lateinit var linearhead: LinearLayout

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = NotpaidScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root

        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        userid = sharedPreferences.getString("userid", "").toString()
        nodata_text = binding.nodataText
        linearhead = binding.linearhead

        recyclerview_paid = binding.notPaidinvoiceRecler
        progressBar_invoice = binding.progressBarinvoice

//        binding.paynowText.setOnClickListener {
//
//            val mDialogView =
//                LayoutInflater.from(activity as Activity).inflate(R.layout.bankdetails_screen, null)
//            //AlertDialogBuilder
//            val mBuilder = AlertDialog.Builder(activity as Activity)
//                .setView(mDialogView)
//                .setTitle("Technician Profile")
//            //show dialog
//            val mAlertDialog = mBuilder.show()
//
//
//            //login button click of custom layout
////            mDialogView.dialogupdateBtn.setOnClickListener {
////                //dismiss dialog
////                mAlertDialog.dismiss()
////                //get text from EditTexts of custom layout
////
////
////
////            }
//
//        }
        binding.paidText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_invoices)
        }
        binding.ppPaidText.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    requireActivity() as Activity,
                    R.id.nav_host_fragment_content_home
                )
            navController.navigate(R.id.navigation_partialpaid)
        }

        getNotPaidInvoiceList()

        return root
    }

    private fun getNotPaidInvoiceList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.alltypes_invoices(getString(R.string.api_key), userid, "2")

            progressBar_invoice.visibility = View.VISIBLE
            call.enqueue(object : Callback<InvoiceResponse> {
                override fun onResponse(
                    call: Call<InvoiceResponse>,
                    response: Response<InvoiceResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    progressBar_invoice.visibility = View.GONE

                    if (response.isSuccessful) {


                        //Set the Adapter to the RecyclerView//

                        try {


                            val activeticketslist =
                                response.body()?.response!!


                            if (activeticketslist.isEmpty()) {
                                nodata_text.visibility = View.VISIBLE
                                linearhead.visibility = View.GONE
                                recyclerview_paid.visibility = View.GONE
                            } else {

                                nodata_text.visibility = View.GONE
                                linearhead.visibility = View.VISIBLE
                                recyclerview_paid.visibility = View.VISIBLE

                                notpaidinvoice_adapter =
                                    NotPaidInvoice_Adapter(
                                        activity as Activity,
                                        activeticketslist as ArrayList<InvoiceListResponse>
                                    )
                                recyclerview_paid.adapter =
                                    notpaidinvoice_adapter
                                notpaidinvoice_adapter.notifyDataSetChanged()
                            }


                    } catch (e : NullPointerException){
                        e.printStackTrace()
                    } catch (e : Exception){
                        e.printStackTrace()
                    }
                }
                }

                override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    progressBar_invoice.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}